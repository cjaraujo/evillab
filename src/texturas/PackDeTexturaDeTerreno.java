package texturas;

public class PackDeTexturaDeTerreno {
	private Textura texturaDeFundo;
	private Textura texturaR;
	private Textura texturaB;
	private Textura texturaG;
	public PackDeTexturaDeTerreno(Textura texturaDeFundo, Textura texturaR, Textura texturaB,
			Textura texturaG) {
		this.texturaDeFundo = texturaDeFundo;
		this.texturaR = texturaR;
		this.texturaB = texturaB;
		this.texturaG = texturaG;
	}
	public Textura getTexturaDeFundo() {
		return texturaDeFundo;
	}
	public Textura getTexturaR() {
		return texturaR;
	}
	public Textura getTexturaB() {
		return texturaB;
	}
	public Textura getTexturaG() {
		return texturaG;
	}
	
}
