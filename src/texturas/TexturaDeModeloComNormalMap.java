package texturas;

public class TexturaDeModeloComNormalMap extends TexturaDeModelo{
	private int normalMap;
	public TexturaDeModeloComNormalMap(int id, int normalMap) {
		super(id);
		this.normalMap = normalMap;
	}
	public int getNormalMap() {
		return normalMap;
	}
	

}
