package texturas;

public class TexturaDeModelo extends Textura{


	private float brilho = 1;
	private float reflexibilidade =0;

	private boolean transparencia = false;
	
	private boolean iluminacaoFalsa = false;
	
	private int numeroDeColunas  = 1;

	public TexturaDeModelo(int id) {
		super(id);
	}

	public float getBrilho() {
		return brilho;
	}

	public void setBrilho(float brilho) {
		this.brilho = brilho;
	}

	public float getReflexibilidade() {
		return reflexibilidade;
	}

	public void setReflexibilidade(float reflexibilidade) {
		this.reflexibilidade = reflexibilidade;
	}

	public boolean isTransparencia() {
		return transparencia;
	}

	public void setTransparencia(boolean transparencia) {
		this.transparencia = transparencia;
	}

	public boolean isIluminacaoFalsa() {
		return iluminacaoFalsa;
	}

	public void setIluminacaoFalsa(boolean iluminacaoFalsa) {
		this.iluminacaoFalsa = iluminacaoFalsa;
	}

	public int getNumeroDeColunas() {
		return numeroDeColunas;
	}

	public void setNumeroDeColunas(int numeroDeColunas) {
		this.numeroDeColunas = numeroDeColunas;
	}
	

}
