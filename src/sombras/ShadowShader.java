package sombras;

import org.lwjgl.util.vector.Matrix4f;

import shaders.AProgrmacaoShader;


public class ShadowShader extends AProgrmacaoShader {
	
	private static final String VERTEX_FILE = "/sombras/shadowVertexShader.txt";
	private static final String FRAGMENT_FILE = "/sombras/shadowFragmentShader.txt";
	
	private int location_mvpMatrix;

	protected ShadowShader() {
		super(VERTEX_FILE, FRAGMENT_FILE);
	}


	
	protected void loadMvpMatrix(Matrix4f mvpMatrix){
		super.carregarMatrix(location_mvpMatrix, mvpMatrix);
	}


	@Override
	protected void getTodasLocalizacoesUniformes() {
		location_mvpMatrix = super.getLocalizacaoUniforme("mvpMatrix");
		
	}

	@Override
	protected void atrelarAtributos() {
		super.atrelarAtributo(0, "in_position");
		
	}

}
