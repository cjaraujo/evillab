#version 400

in vec3 textureCoords;
out vec4 out_Color;

uniform samplerCube cubeMap;
uniform vec3 corDoCeu;
const float limiteChao =-1.0;
const float limiteCeu = 70.0;
void main(void){
	vec4 corFinal = texture(cubeMap,textureCoords);
	float fator = (textureCoords.y - limiteChao)/(limiteCeu- limiteChao);
	fator = clamp(fator,0.0,1.0);
	
    out_Color = corFinal;
}