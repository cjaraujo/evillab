package shaders;

import java.util.ArrayList;

import org.lwjgl.util.vector.Matrix4f;

public class ShaderDeGui extends AProgrmacaoShader{
	private static final String ARQUIVO_VERTEX = "/shaders/ShaderVetexGui.txt";
	private static final String ARQUIVO_FRAGMENTO = "/shaders/ShaderFragmentadoGui.txt";
	private int localizacao_matrixTransformacao;
	
	public ShaderDeGui() {
		super(ARQUIVO_VERTEX, ARQUIVO_FRAGMENTO);
		
	}

	@Override
	protected void getTodasLocalizacoesUniformes() {
		
		localizacao_matrixTransformacao = super.getLocalizacaoUniforme("matrixTransformacao");
	}

	@Override
	protected void atrelarAtributos() {
		super.atrelarAtributo(0, "position");
		
	}
	
	public void carregarMatrixDeTransformacao(Matrix4f matrix) {
		super.carregarMatrix(localizacao_matrixTransformacao, matrix);
	}

}
