package shaders;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.List;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;

public abstract class AProgrmacaoShader {
	private int programaId;
	private int verticeShaderId;
	private int shaderFragmentadoId;
	private List<String> variaveisMagicasChave = new ArrayList<String>();
	private List<String> variaveisMagicasValor = new ArrayList<String>();
	private static FloatBuffer matrixBuffer = BufferUtils.createFloatBuffer(16);

	public AProgrmacaoShader(String arquivoDoVertice, String arquivoDoFragmento) {
		verticeShaderId = carregarShader(arquivoDoVertice, GL20.GL_VERTEX_SHADER);
		shaderFragmentadoId = carregarShader(arquivoDoFragmento, GL20.GL_FRAGMENT_SHADER);
		programaId = GL20.glCreateProgram();
		GL20.glAttachShader(programaId, verticeShaderId);
		GL20.glAttachShader(programaId, shaderFragmentadoId);
		atrelarAtributos();
		GL20.glLinkProgram(programaId);
		GL20.glValidateProgram(programaId);
		getTodasLocalizacoesUniformes();
	}

	protected int getLocalizacaoUniforme(String nomeUniforme) {
		return GL20.glGetUniformLocation(programaId, nomeUniforme);
	}
	

	protected abstract void getTodasLocalizacoesUniformes();

	protected void carregarFloat(int localizacao_shineDumper, float f) {
		GL20.glUniform1f(localizacao_shineDumper, f);
	}
	protected void carrefarInt(int localizacao, int valor) {
		GL20.glUniform1i(localizacao, valor);
	}

	protected void carregarBoleano(int localizacao, boolean valor) {
		float aCarregar = 0;
		if (valor) {
			aCarregar = 1;
		}
		GL20.glUniform1f(localizacao, aCarregar);

	}

	protected void carregarMatrix(int localizacao, Matrix4f matrix) {
		matrix.store(matrixBuffer);
		matrixBuffer.flip();
		GL20.glUniformMatrix4(localizacao, false, matrixBuffer);
	}

	protected void carregarVetor(int localizacao, Vector3f vetor) {
		GL20.glUniform3f(localizacao, vetor.x, vetor.y, vetor.z);
	}
	protected void carregarVetor(int localizacao, Vector4f vetor) {
		GL20.glUniform4f(localizacao, vetor.x, vetor.y, vetor.z,vetor.w);
	}
	protected void carregarVetor2d(int localizacao, Vector2f vetor) {
		GL20.glUniform2f(localizacao, vetor.x, vetor.y);
	}

	public void comecar() {
		GL20.glUseProgram(programaId);
	}

	public void parar() {
		GL20.glUseProgram(0);
	}

	protected void atrelarAtributo(int atributo, String nomeDaVariavel) {
		GL20.glBindAttribLocation(programaId, atributo, nomeDaVariavel);
	}
	private static String variaveisMagicas(String string,List<String> variaveisMagicasChave,List<String> variaveisMagicasValor) {
		for(int i = 0;i<variaveisMagicasChave.size();i++) {
			string.replaceAll(variaveisMagicasChave.get(i), variaveisMagicasValor.get(i));
		}
		return string;
	}

	public void limpar() {
		parar();
		GL20.glDetachShader(programaId, verticeShaderId);
		GL20.glDetachShader(programaId, shaderFragmentadoId);
		GL20.glDeleteShader(verticeShaderId);
		GL20.glDeleteShader(shaderFragmentadoId);
		GL20.glDeleteProgram(programaId);
	}

	protected abstract void atrelarAtributos();

	@SuppressWarnings("deprecation")
	private static int carregarShader(String arquivo, int tipo) {
		StringBuilder fonteDoShader = new StringBuilder();
		try {
			InputStream in = Class.class.getResourceAsStream(arquivo);
			BufferedReader leitor = new BufferedReader(new InputStreamReader(in));
			String linha;
			while ((linha = leitor.readLine()) != null) {
				
				fonteDoShader.append(linha).append("\n");
			}
			leitor.close();
		} catch (IOException e) {
			System.err.println("N�o foi poss�vel ler o arquivo");
			e.printStackTrace();
			System.exit(-1);
		}
		int shaderId = GL20.glCreateShader(tipo);
		GL20.glShaderSource(shaderId, fonteDoShader);
		GL20.glCompileShader(shaderId);
		if (GL20.glGetShader(shaderId, GL20.GL_COMPILE_STATUS) == GL11.GL_FALSE) {
			System.out.println(GL20.glGetShaderInfoLog(shaderId, 500));
			System.out.println("N�o foi possivel compilar shader.");
			System.exit(-1);
		}
		return shaderId;
	}
}
