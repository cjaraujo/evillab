package shaders;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;

import ferramentas.Calculos;
import nodes.Camera;
import nodes.EntidadeSimples;
import nodes.Luz;
import nodes.LuzLocal;
import texturas.TexturaDeModelo;

public class ShaderDeTerreno extends AProgrmacaoShader  {
	private static final int QUANTIDADE_LUZ = 4;
	private static final String ARQUIVO_VERTICES = "/shaders/ShaderDeVertexDeTerreno.txt";
	private static final String ARQUIVO_FRAGMENTADO = "/shaders/ShaderFragmentadoDeTerreno.txt";
	
	private int localizacao_transformationMatrix;
	private int localizacao_projectionMatrix;
	private int localizacao_viewMatrix;
	private int localizacao_lightPosition[];
	private int localizacao_lightColour[];
	private int localizacao_shineDumper;
	private int localiazacao_reflectivity;
	private int localizacao_skyColour;
	private int localizacao_backgroundTexture;
	private int localizacao_rTexture;
	private int localizacao_gTexture;
	private int localizacao_bTexture;
	private int localizacao_blendMap;
	private int localizacao_atenuacao[];
	private int localizacao_plano;
	private int localizacao_intensidade[];
	private int localizacao_toShadowMapSpace;
	private int localizacao_shadowMap;
	public ShaderDeTerreno() {

		
		super(ARQUIVO_VERTICES,ARQUIVO_FRAGMENTADO);
	}

	protected void atrelarAtributos() {
		super.atrelarAtributo(0, "position");
		super.atrelarAtributo(1, "textureCoords");
		super.atrelarAtributo(2, "normal");
	}
	@Override
	protected void getTodasLocalizacoesUniformes() {
		// TODO Auto-generated method stub
		localizacao_transformationMatrix=super.getLocalizacaoUniforme("transformationMatrix");
		localizacao_projectionMatrix = super.getLocalizacaoUniforme("projectionMatrix");
		localizacao_viewMatrix = super.getLocalizacaoUniforme("viewMatrix");
		localizacao_shineDumper= super.getLocalizacaoUniforme("shineDamper");
		localiazacao_reflectivity= super.getLocalizacaoUniforme("reflectivity");
		localizacao_skyColour = super.getLocalizacaoUniforme("skyColour");
		localizacao_backgroundTexture = super.getLocalizacaoUniforme("backgroundTexture");
		localizacao_rTexture = super.getLocalizacaoUniforme("rTexture");
		localizacao_gTexture = super.getLocalizacaoUniforme("gTexture");
		localizacao_bTexture = super.getLocalizacaoUniforme("bTexture");
		localizacao_blendMap = super.getLocalizacaoUniforme("blendMap");
		localizacao_plano = super.getLocalizacaoUniforme("plano");
		localizacao_toShadowMapSpace = getLocalizacaoUniforme("toShadowMapSpace");
		localizacao_shadowMap = getLocalizacaoUniforme("shadowMap");
		localizacao_lightPosition = new int[QUANTIDADE_LUZ];
		localizacao_lightColour = new int[QUANTIDADE_LUZ];
		localizacao_atenuacao = new int[QUANTIDADE_LUZ];
		localizacao_intensidade = new int[QUANTIDADE_LUZ];
		for(int i= 0; i<QUANTIDADE_LUZ; i++) {
			localizacao_lightPosition[i] = super.getLocalizacaoUniforme("lightPosition["+i+"]");
			localizacao_lightColour[i] = super.getLocalizacaoUniforme("lightColour["+i+"]");
			localizacao_atenuacao[i]=super.getLocalizacaoUniforme("atenuacao["+i+"]");
			localizacao_intensidade[i]=super.getLocalizacaoUniforme("intensidade["+i+"]");
		}
	}
	public void conectarUnidadesDeTextura() {
		super.carrefarInt(localizacao_backgroundTexture, 0);
		super.carrefarInt(localizacao_rTexture, 1);
		super.carrefarInt(localizacao_gTexture, 2);
		super.carrefarInt(localizacao_bTexture, 3);
		super.carrefarInt(localizacao_blendMap, 4);
		super.carrefarInt(localizacao_shadowMap, 5);
	}
	public void carregarCorDoCeu(float r, float g, float b) {
		super.carregarVetor(localizacao_skyColour, new Vector3f(r,g,b));
	}
	public void carregarSombras(Matrix4f sombras) {
		super.carregarMatrix(localizacao_toShadowMapSpace, sombras);
	}
	public void carregarTransformacaoMatrix(Matrix4f matrix) {
		super.carregarMatrix(localizacao_transformationMatrix, matrix);
	}
	public void carregarLuzes(List<Luz> luzes) {
		for(int i = 0;i<QUANTIDADE_LUZ;i++) {
			if(i<luzes.size()) {
				super.carregarVetor(localizacao_lightPosition[i], luzes.get(i).getPosicaoGlobal());
				super.carregarVetor(localizacao_lightColour[i], luzes.get(i).getCor());
				super.carregarFloat(localizacao_intensidade[i], luzes.get(i).getIntensidade());
				if(luzes.get(i) instanceof LuzLocal) {
					super.carregarVetor(localizacao_atenuacao[i], ((LuzLocal)luzes.get(i)).getAtenuacao());
					continue;
				}
				super.carregarVetor(localizacao_atenuacao[i], new Vector3f(1,0,0));
			}else {
				super.carregarVetor(localizacao_lightPosition[i], new Vector3f(0,0,0));
				super.carregarVetor(localizacao_lightColour[i], new Vector3f(0,0,0));
				super.carregarVetor(localizacao_atenuacao[i], new Vector3f(1,0,0));
				super.carregarFloat(localizacao_intensidade[i], 1);
			}
		
		}
	}
	public void carregarPlano(Vector4f plano) {
		super.carregarVetor(localizacao_plano, plano);
	}
	public void carregarMatrixDeVisao(Camera camera) {
		Matrix4f matrixDeVisao = Calculos.criarMatrixDeVisao(camera);
		super.carregarMatrix(localizacao_viewMatrix, matrixDeVisao);
	}
	public void carregarMatrixDeProjecao(Matrix4f projecao) {
		super.carregarMatrix(localizacao_projectionMatrix, projecao);
	}
	public void carregarBrilho(TexturaDeModelo modelo) {
		super.carregarFloat(localizacao_shineDumper, modelo.getBrilho());
		super.carregarFloat(localiazacao_reflectivity, modelo.getReflexibilidade());
	}
	public void carregarBrilho(float brilho, float reflexibilidade) {
		super.carregarFloat(localizacao_shineDumper, brilho);
		super.carregarFloat(localiazacao_reflectivity, reflexibilidade);
	}

}
