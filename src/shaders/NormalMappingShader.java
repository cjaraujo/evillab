package shaders;

import java.util.List;

import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;

import nodes.Luz;
import nodes.LuzLocal;


public class NormalMappingShader extends AProgrmacaoShader{
	
	private static final int MAX_LIGHTS = 4;
	
	private static final String VERTEX_FILE = "/shaders/normalMapVShader.txt";
	private static final String FRAGMENT_FILE = "/shaders/normalMapFShader.txt";
	
	private int location_transformationMatrix;
	private int location_projectionMatrix;
	private int location_viewMatrix;
	private int location_lightPositionEyeSpace[];
	private int location_lightColour[];
	private int location_attenuation[];
	private int location_shineDamper;
	private int location_reflectivity;
	private int location_skyColour;
	private int location_numberOfRows;
	private int location_offset;
	private int location_plane;
	private int location_modelTexture;
	private int location_normalMap;
	private int localizacao_intensidade[];

	public NormalMappingShader() {
		super(VERTEX_FILE, FRAGMENT_FILE);
	}

	@Override
	protected void atrelarAtributos() {
		super.atrelarAtributo(0, "position");
		super.atrelarAtributo(1, "textureCoordinates");
		super.atrelarAtributo(2, "normal");
		super.atrelarAtributo(3, "tangent");
	}

	@Override
	protected void getTodasLocalizacoesUniformes() {
		location_transformationMatrix = super.getLocalizacaoUniforme("transformationMatrix");
		location_projectionMatrix = super.getLocalizacaoUniforme("projectionMatrix");
		location_viewMatrix = super.getLocalizacaoUniforme("viewMatrix");
		location_shineDamper = super.getLocalizacaoUniforme("shineDamper");
		location_reflectivity = super.getLocalizacaoUniforme("reflectivity");
		location_skyColour = super.getLocalizacaoUniforme("skyColour");
		location_numberOfRows = super.getLocalizacaoUniforme("numberOfRows");
		location_offset = super.getLocalizacaoUniforme("offset");
		location_plane = super.getLocalizacaoUniforme("plane");
		location_modelTexture = super.getLocalizacaoUniforme("modelTexture");
		location_normalMap = super.getLocalizacaoUniforme("normalMap");
		location_lightPositionEyeSpace = new int[MAX_LIGHTS];
		location_lightColour = new int[MAX_LIGHTS];
		location_attenuation = new int[MAX_LIGHTS];
		localizacao_intensidade = new int[MAX_LIGHTS];
		for(int i=0;i<MAX_LIGHTS;i++){
			location_lightPositionEyeSpace[i] = super.getLocalizacaoUniforme("lightPositionEyeSpace[" + i + "]");
			location_lightColour[i] = super.getLocalizacaoUniforme("lightColour[" + i + "]");
			location_attenuation[i] = super.getLocalizacaoUniforme("attenuation[" + i + "]");
			localizacao_intensidade[i] = super.getLocalizacaoUniforme("intensidade[" + i + "]");
		}
	}
	
	public void connectTextureUnits(){
		super.carrefarInt(location_modelTexture, 0);
		super.carrefarInt(location_normalMap, 1);
	}
	
	public void loadClipPlane(Vector4f plane){
		super.carregarVetor(location_plane, plane);
	}
	
	public void loadNumberOfRows(int numberOfRows){
		super.carregarFloat(location_numberOfRows, numberOfRows);
	}
	
	public void loadOffset(float x, float y){
		super.carregarVetor2d(location_offset, new Vector2f(x,y));
	}
	
	public void loadSkyColour(float r, float g, float b){
		super.carregarVetor(location_skyColour, new Vector3f(r,g,b));
	}
	
	public void loadShineVariables(float damper,float reflectivity){
		super.carregarFloat(location_shineDamper, damper);
		super.carregarFloat(location_reflectivity, reflectivity);
	}
	
	public void loadTransformationMatrix(Matrix4f matrix){
		super.carregarMatrix(location_transformationMatrix, matrix);
	}
	
	public void loadLights(List<Luz> lights, Matrix4f viewMatrix){
		for(int i=0;i<MAX_LIGHTS;i++){
			if(i<lights.size()){
				super.carregarVetor(location_lightPositionEyeSpace[i], getEyeSpacePosition(lights.get(i), viewMatrix));
				super.carregarVetor(location_lightColour[i], lights.get(i).getCor());
				super.carregarFloat(localizacao_intensidade[i], lights.get(i).getIntensidade());
				if(lights.get(i) instanceof LuzLocal) {
					super.carregarVetor(location_attenuation[i], ((LuzLocal) lights.get(i)).getAtenuacao());
					continue;
				}
				super.carregarVetor(location_attenuation[i], new Vector3f(1, 0, 0));
			}else{
				super.carregarVetor(location_lightPositionEyeSpace[i], new Vector3f(0, 0, 0));
				super.carregarVetor(location_lightColour[i], new Vector3f(0, 0, 0));
				super.carregarVetor(location_attenuation[i], new Vector3f(1, 0, 0));
				super.carregarFloat(localizacao_intensidade[i], 1);
			}
		}
	}
	
	public void loadViewMatrix(Matrix4f viewMatrix){
		super.carregarMatrix(location_viewMatrix, viewMatrix);
	}
	
	public void loadProjectionMatrix(Matrix4f projection){
		super.carregarMatrix(location_projectionMatrix, projection);
	}
	
	private Vector3f getEyeSpacePosition(Luz light, Matrix4f viewMatrix){
		Vector3f position = light.getPosicaoGlobal();
		Vector4f eyeSpacePos = new Vector4f(position.x,position.y, position.z, 1f);
		Matrix4f.transform(viewMatrix, eyeSpacePos, eyeSpacePos);
		return new Vector3f(eyeSpacePos);
	}
	
	

}
