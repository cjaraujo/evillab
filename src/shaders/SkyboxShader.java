package shaders;

import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;

import ferramentas.Calculos;
import nodes.Camera;
;

public class SkyboxShader extends AProgrmacaoShader{

	private static final String VERTEX_FILE = "/shaders/skyboxVertexShader.txt";
	private static final String FRAGMENT_FILE = "/shaders/skyboxFragmentShader.txt";
	
	private int location_projectionMatrix;
	private int location_viewMatrix;
	private int localizacao_plano;
	private int localizacao_corDoCeu;
	public SkyboxShader() {
		super(VERTEX_FILE, FRAGMENT_FILE);
	}
	
	public void loadProjectionMatrix(Matrix4f matrix){
		super.carregarMatrix(location_projectionMatrix, matrix);
	}

	public void loadViewMatrix(Camera camera){
		Matrix4f matrix = Calculos.criarMatrixDeVisao(camera);
		matrix.m30 = 0;
		matrix.m31 = 0;
		matrix.m32 = 0;
		super.carregarMatrix(location_viewMatrix, matrix);
	}
	
	public void carregarPlano(Vector4f plano) {
		super.carregarVetor(localizacao_plano, plano);
	}
	@Override
	protected void getTodasLocalizacoesUniformes() {
		location_projectionMatrix = super.getLocalizacaoUniforme("projectionMatrix");
		location_viewMatrix = super.getLocalizacaoUniforme("viewMatrix");
		localizacao_plano = super.getLocalizacaoUniforme("plano");
		localizacao_corDoCeu = super.getLocalizacaoUniforme("corDoCeu");
	}

	@Override
	protected void atrelarAtributos() {
		super.atrelarAtributo(0, "position");
	}
	public void carregarCorDoCeu(Vector3f corDoCeu) {
		super.carregarVetor(localizacao_corDoCeu, corDoCeu);
	}

}
