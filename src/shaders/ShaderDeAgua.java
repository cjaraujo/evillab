package shaders;

import java.util.List;

import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;

import nodes.Camera;
import nodes.Luz;
import nodes.LuzLocal;
import ferramentas.Calculos;

public class ShaderDeAgua extends AProgrmacaoShader {

	private final static String VERTEX_FILE = "/shaders/waterVertex.txt";
	private final static String FRAGMENT_FILE = "/shaders/waterFragment.txt";

	private int location_modelMatrix;
	private int location_viewMatrix;
	private int location_projectionMatrix;
	private int localizacao_reflectionTexture;
	private int localizacao_dudvMap;
	private int localizacao_moveFactor;
	private int localizacao_cameraPosition;
	private int localizacao_normalMap;
	private int localizacao_lightPosition[];
	private int localizacao_lightColour[];
	private int localizacao_atenuacao[];
	private int localizacao_intensidade[];
	private int localizacao_depthMap;
	
	private int localizacao_refractionTexture;
	public ShaderDeAgua() {
		super(VERTEX_FILE, FRAGMENT_FILE);
	}



	public void carregarMatrixDeProjecao(Matrix4f projection) {
		super.carregarMatrix(location_projectionMatrix, projection);
	}
	
	public void carregarMatrixDeVisao(Camera camera){
		Matrix4f viewMatrix = Calculos.criarMatrixDeVisao(camera);
		super.carregarMatrix(location_viewMatrix, viewMatrix);
		super.carregarVetor(localizacao_cameraPosition, camera.getPosicaoGlobal());
	}

	public void carregarMatrixDeModelo(Matrix4f modelMatrix){
		super.carregarMatrix(location_modelMatrix, modelMatrix);
	}

	@Override
	protected void getTodasLocalizacoesUniformes() {
		location_projectionMatrix = super.getLocalizacaoUniforme("projectionMatrix");
		location_viewMatrix = super.getLocalizacaoUniforme("viewMatrix");
		location_modelMatrix = super.getLocalizacaoUniforme("modelMatrix");
		localizacao_reflectionTexture = super.getLocalizacaoUniforme("reflectionTexture");
		localizacao_refractionTexture = super.getLocalizacaoUniforme("refractionTexture");
		localizacao_dudvMap = super.getLocalizacaoUniforme("dudvMap");
		localizacao_moveFactor = super.getLocalizacaoUniforme("moveFactor");
		localizacao_cameraPosition = super.getLocalizacaoUniforme("cameraPosition");
		localizacao_normalMap = super.getLocalizacaoUniforme("normalMap");
		localizacao_depthMap = super.getLocalizacaoUniforme("depthMap");
		localizacao_lightPosition = new int[4];
		localizacao_lightColour = new int[4];
		localizacao_atenuacao = new int[4];
		localizacao_intensidade = new int[4];
		for(int i= 0; i<4; i++) {
			localizacao_lightPosition[i] = super.getLocalizacaoUniforme("lightPosition["+i+"]");
			localizacao_lightColour[i] = super.getLocalizacaoUniforme("lightColour["+i+"]");
			localizacao_atenuacao[i]=super.getLocalizacaoUniforme("atenuacao["+i+"]");
			localizacao_intensidade[i]=super.getLocalizacaoUniforme("intensidade["+i+"]");
		}
	}
	public void conectarUnidadesDeTextura() {
		super.carrefarInt(localizacao_reflectionTexture, 0);
		super.carrefarInt(localizacao_refractionTexture, 1);
		super.carrefarInt(localizacao_dudvMap, 2);
		super.carrefarInt(localizacao_normalMap, 3);
		super.carrefarInt(localizacao_depthMap, 4);
	}
	public void carregarLuzes(List<Luz> luzes) {
		for(int i = 0;i<4;i++) {
			if(i<luzes.size()) {
				super.carregarVetor(localizacao_lightPosition[i], luzes.get(i).getPosicaoGlobal());
				super.carregarVetor(localizacao_lightColour[i], luzes.get(i).getCor());
				super.carregarFloat(localizacao_intensidade[i], luzes.get(i).getIntensidade());
				if(luzes.get(i) instanceof LuzLocal) {
					super.carregarVetor(localizacao_atenuacao[i], ((LuzLocal)luzes.get(i)).getAtenuacao());
					continue;
				}
				super.carregarVetor(localizacao_atenuacao[i], new Vector3f(1,0,0));
			}else {
				super.carregarVetor(localizacao_lightPosition[i], new Vector3f(0,0,0));
				super.carregarVetor(localizacao_lightColour[i], new Vector3f(0,0,0));
				super.carregarVetor(localizacao_atenuacao[i], new Vector3f(1,0,0));
				super.carregarFloat(localizacao_intensidade[i], 1);
			}
		
		}
	}
	public void carregarFatorDeMovimento(float fatorDeMovimento) {
		super.carregarFloat(localizacao_moveFactor, fatorDeMovimento);
	}

	@Override
	protected void atrelarAtributos() {
		super.atrelarAtributo(0, "position");
		
	}

}
