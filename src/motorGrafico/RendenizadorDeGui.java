package motorGrafico;

import java.util.Collection;
import java.util.List;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.lwjgl.util.vector.Matrix4f;

import ferramentas.Calculos;
import modelos.ModeloDeLinha;
import nodes.Gui;
import shaders.ShaderDeGui;

public class RendenizadorDeGui {
	private ShaderDeGui shader;
	private final ModeloDeLinha modeloDeLinha;

	public RendenizadorDeGui(Carregador carregador) {
		float[] posicoes = { -1, 1, -1, -1, 1, 1, 1, -1 };
		modeloDeLinha = carregador.carregarAoVAO(posicoes);
		shader = new ShaderDeGui();
	}

	public void rendenizar(Collection<Gui> collection) {
		shader.comecar();
		GL30.glBindVertexArray(modeloDeLinha.getVaoId());
		GL20.glEnableVertexAttribArray(0);
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		GL11.glDisable(GL11.GL_DEPTH_TEST);
		for (Gui gui : collection) {
			GL13.glActiveTexture(GL13.GL_TEXTURE0);
			GL11.glBindTexture(GL11.GL_TEXTURE_2D, gui.getTextura().getId());
			Matrix4f matrix = Calculos.criarTranformacaoMatrix(gui.getPosicao(), gui.getEscala());
			shader.carregarMatrixDeTransformacao(matrix);
			GL11.glDrawArrays(GL11.GL_TRIANGLE_STRIP, 0, modeloDeLinha.getQuantidadeVertex());
		}
		GL11.glEnable(GL11.GL_DEPTH_TEST);
		GL11.glDisable(GL11.GL_BLEND);
		GL20.glDisableVertexAttribArray(0);
		GL30.glBindVertexArray(0);
		shader.parar();
	}

	public void limpar() {
		shader.limpar();
	}
}
