package motorGrafico;

import shaders.ShaderDeAgua;
import shaders.ShaderDeTerreno;
import shaders.ShaderEstatico;
import sombras.ShadowMapMasterRenderer;
import texturas.Textura;
import nodes.Terreno;

import java.util.List;
import java.util.Map;

import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL30;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;

import agua.FrameDeBufferDeAgua;
import modelos.ModeloTexturizado;
import modelos.ModeloTexturizadoComNormalMap;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import motorGrafico.RendenizadorDeEntidades;
import nodes.Camera;
import nodes.EntidadeModelada;
import nodes.EntidadeModeladaComNormalMap;
import nodes.Gui;
import nodes.Luz;
import nodes.Raiz;

public class RendenizadorMestre {
	public static final float FOV = 70;
	public static final float NEAR_PLANE = 0.1f;
	public static final float FAR_PLANE = 4000;

	public static final float RED = 0.1f;
	public static final float GREEN = 0.5f;
	public static final float BLUE = 0.7f;

	private ShaderEstatico shader = new ShaderEstatico();
	private ShaderDeAgua shaderDeAgua = new ShaderDeAgua();
	private RendenizadorDeEntidades rendenizador;
	private RendenizadorDeTerreno rendenizadorDeTerreno;
	private ShaderDeTerreno shaderDeTerreno = new ShaderDeTerreno();
	private Matrix4f matrixDeProjecao = new Matrix4f();
	private RendenizadorDeAgua rendenizadorDeAgua;
	private RendenizadorNormalMap rendenizadorNormalMap;
	private RendenizadorCeu rendenizadorCeu;

	public RendenizadorMestre(Carregador carregador, FrameDeBufferDeAgua frameDeBufferDeAgua) {
		ativarCulling();

		criarMatrixDeProjecao();
		rendenizadorCeu = new RendenizadorCeu(carregador, matrixDeProjecao);
		this.rendenizador = new RendenizadorDeEntidades(shader, matrixDeProjecao);
		this.rendenizadorDeTerreno = new RendenizadorDeTerreno(shaderDeTerreno, matrixDeProjecao);
		rendenizadorDeAgua = new RendenizadorDeAgua(carregador, shaderDeAgua, matrixDeProjecao, frameDeBufferDeAgua);
		rendenizadorNormalMap = new RendenizadorNormalMap(matrixDeProjecao);
	}

	public Matrix4f getMatrixDeProjecao() {
		return matrixDeProjecao;
	}

	public RendenizadorDeAgua getRendenizadorDeAgua() {
		return rendenizadorDeAgua;
	}

	public static void ativarCulling() {
		GL11.glEnable(GL11.GL_CULL_FACE);
		GL11.glCullFace(GL11.GL_BACK);
	}

	public static void desativarCulling() {
		GL11.glDisable(GL11.GL_CULL_FACE);
	}

	public void rendenizar(List<Luz> luzes, List<Terreno> terrenos,
			Map<ModeloTexturizado, List<EntidadeModelada>> entidades,
			Map<ModeloTexturizadoComNormalMap, List<EntidadeModeladaComNormalMap>> entidadesComNormalMap, Camera camera,
			Vector4f plano, ShadowMapMasterRenderer sombras) {
		preparar();

		shader.comecar();
		shader.carregarPlano(plano);
		shader.carregarCorDoCeu(RED, GREEN, BLUE);
		shader.carregarLuzes(luzes);
		shader.carregarMatrixDeVisao(camera);
		rendenizador.rendenizar(entidades);
		shader.parar();
		
		rendenizadorNormalMap.render(entidadesComNormalMap, plano, luzes, camera);

		shaderDeTerreno.comecar();
		shaderDeTerreno.carregarPlano(plano);
		shaderDeTerreno.carregarCorDoCeu(RED, GREEN, BLUE);
		shaderDeTerreno.carregarLuzes(luzes);
		shaderDeTerreno.carregarMatrixDeVisao(camera);
		rendenizadorDeTerreno.rendenizar(terrenos, sombras);
		shaderDeTerreno.parar();
		GL11.glDisable(GL30.GL_CLIP_DISTANCE0);
		rendenizadorCeu.rendenizar(camera, plano);
	}

	public void limpar() {
		shader.limpar();
		shaderDeTerreno.limpar();
		rendenizadorNormalMap.limpar();
	}

	public void preparar() {
		GL11.glEnable(GL11.GL_DEPTH_TEST);
		GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
		GL11.glClearColor(RED, GREEN, BLUE, 1);
	}

	private void criarMatrixDeProjecao() {
		float aspectRatio = (float) Display.getWidth() / (float) Display.getHeight();
		float y_scale = (float) ((1f / Math.tan(Math.toRadians(FOV / 2f))));
		float x_scale = y_scale / aspectRatio;
		float frustum_length = FAR_PLANE - NEAR_PLANE;

		matrixDeProjecao.m00 = x_scale;
		matrixDeProjecao.m11 = y_scale;
		matrixDeProjecao.m22 = -((FAR_PLANE + NEAR_PLANE) / frustum_length);
		matrixDeProjecao.m23 = -1;
		matrixDeProjecao.m32 = -((2 * NEAR_PLANE * FAR_PLANE) / frustum_length);
		matrixDeProjecao.m33 = 0;
	}
}
