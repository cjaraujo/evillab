package motorGrafico;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

import modelos.ModeloDeLinha;

public class CarregadorObj {
	public static ModeloDeLinha carregarModeloObj(String nomeArquivo, Carregador carregador) {
		FileReader fl = null;
		try {
			fl = new FileReader(new File("res/" + nomeArquivo + ".obj"));
		} catch (FileNotFoundException e) {
			System.err.println("Arquivo n�o encontrado");
			e.printStackTrace();
		}
		BufferedReader leitor = new BufferedReader(fl);
		String linha;
		List<Vector3f> vertices = new ArrayList<Vector3f>();
		List<Vector2f> texturas = new ArrayList<Vector2f>();
		List<Vector3f> normals = new ArrayList<Vector3f>();
		List<Integer> indices = new ArrayList<Integer>();
		float[] verticesArray = null;
		float[] normalsArray = null;
		float[] texturasArray = null;
		int[] indicesArray = null;
		try {

			while (true) {
				linha = leitor.readLine();
				String[] linhaAtual = linha.split(" ");
				if (linha.startsWith("v ")) {
					Vector3f vertex = new Vector3f(Float.parseFloat(linhaAtual[1]), Float.parseFloat(linhaAtual[2]),
							Float.parseFloat(linhaAtual[3]));
					vertices.add(vertex);
					
				} else if (linha.startsWith("vt ")) {
					Vector2f textura = new Vector2f(Float.parseFloat(linhaAtual[1]), Float.parseFloat(linhaAtual[2]));
					texturas.add(textura);
				} else if (linha.startsWith("vn ")) {
					Vector3f normal = new Vector3f(Float.parseFloat(linhaAtual[1]), Float.parseFloat(linhaAtual[2]),
							Float.parseFloat(linhaAtual[3]));
					normals.add(normal);

				} else if (linha.startsWith("f ")) {
					texturasArray = new float[vertices.size() * 2];
					normalsArray = new float[vertices.size() * 3];
					break;
				}
			}
			while (linha != null) {
				if (!linha.startsWith("f ")) {
					linha = leitor.readLine();
					continue;
				}
				String[] linhaAtual = linha.split(" ");
				String[] vertex1 = linhaAtual[1].split("/");
				String[] vertex2 = linhaAtual[2].split("/");
				String[] vertex3 = linhaAtual[3].split("/");
				processarVertex(vertex1,indices,texturas,normals,texturasArray,normalsArray);
				processarVertex(vertex2,indices,texturas,normals,texturasArray,normalsArray);
				processarVertex(vertex3,indices,texturas,normals,texturasArray,normalsArray);
				linha = leitor.readLine();
				
			}
			leitor.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
		verticesArray = new float[vertices.size()*3];
		indicesArray = new int[indices.size()] ;
		int ponteiroVertex = 0;
		for(Vector3f verte:vertices) {
			verticesArray[ponteiroVertex++] = verte.x;
			verticesArray[ponteiroVertex++] = verte.y;
			verticesArray[ponteiroVertex++] = verte.z;
		}
		for(int i=0;i<indices.size();i++) {
			indicesArray[i] = indices.get(i);
		}
		return carregador.carregarAoVAO(verticesArray, texturasArray,normalsArray, indicesArray);
		
	}

	private static void processarVertex(String[] dadosVertex, List<Integer> indices, List<Vector2f> texturas,
			List<Vector3f> normals, float[] arrayTextura, float[] arrayNormals) {
		int ponteiroVetexAtual = Integer.parseInt(dadosVertex[0]) - 1;
		indices.add(ponteiroVetexAtual);
		Vector2f texturaAtual = texturas.get(Integer.parseInt(dadosVertex[1])-1);
		arrayTextura[ponteiroVetexAtual*2] = texturaAtual.x;
		arrayTextura[ponteiroVetexAtual*2+1]= 1 - texturaAtual.y;
		Vector3f normalAtual = normals.get(Integer.parseInt(dadosVertex[2])-1);
		arrayNormals[ponteiroVetexAtual*3]= normalAtual.x;
		arrayNormals[ponteiroVetexAtual*3+1]= normalAtual.y;
		arrayNormals[ponteiroVetexAtual*3+2]= normalAtual.z;
		
	}

}
