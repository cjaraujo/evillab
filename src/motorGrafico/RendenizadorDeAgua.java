package motorGrafico;

import java.util.List;



import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;

import agua.FrameDeBufferDeAgua;
import ferramentas.Calculos;
import modelos.ModeloDeLinha;
import nodes.Agua;
import nodes.Camera;
import nodes.Luz;
import shaders.ShaderDeAgua;

public class RendenizadorDeAgua {
	private final String dudvMap = "waterDUDV";
	private final String mapaDeNormais = "normalMap";
	private final float VELOCIDADE_ONDA = 0.1f;
	private ModeloDeLinha quad;
	private ShaderDeAgua shader;
	private FrameDeBufferDeAgua frameDeBufferDeAgua;
	private int idDudv;
	private int idMapaDeNormais;
	private float fatorDemovimento = 0;
	public RendenizadorDeAgua(Carregador carregador, ShaderDeAgua shader, Matrix4f projectionMatrix,FrameDeBufferDeAgua frameDeBufferDeAgua) {
		this.shader = shader;
		this.frameDeBufferDeAgua=frameDeBufferDeAgua;
		idDudv = carregador.carregarTextura(dudvMap);
		idMapaDeNormais = carregador.carregarTextura(mapaDeNormais);
		shader.comecar();
		shader.conectarUnidadesDeTextura();
		shader.carregarMatrixDeProjecao(projectionMatrix);
		shader.parar();
		setUpVAO(carregador);
	}

	public void render(List<Agua> water, Camera camera, List<Luz>luzes) {
		prepareRender(camera,luzes);	
		for (Agua tile : water) {
			Matrix4f modelMatrix = Calculos.criarTranformacaoMatrix(
					new Vector3f(tile.getPosicaoGlobal().x, tile.getPosicaoGlobal().y, tile.getPosicaoGlobal().z), 0, 0, 0,
					tile.getTamanho());
			shader.carregarMatrixDeModelo(modelMatrix);
			
			GL11.glDrawArrays(GL11.GL_TRIANGLES, 0, quad.getQuantidadeVertex());
		}
		unbind();
	}
	private void prepareRender(Camera camera, List<Luz>luzes){
		shader.comecar();;
		shader.carregarMatrixDeVisao(camera);
		fatorDemovimento += VELOCIDADE_ONDA*GerenciadorDeTela.obterDelta();
		fatorDemovimento %=1;
		shader.carregarFatorDeMovimento(fatorDemovimento);
		shader.carregarLuzes(luzes);
		GL30.glBindVertexArray(quad.getVaoId());
		GL20.glEnableVertexAttribArray(0);
		GL13.glActiveTexture(GL13.GL_TEXTURE0);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, frameDeBufferDeAgua.getReflectionTexture());
		GL13.glActiveTexture(GL13.GL_TEXTURE1);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, frameDeBufferDeAgua.getRefractionTexture());
		GL13.glActiveTexture(GL13.GL_TEXTURE2);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, idDudv);
		GL13.glActiveTexture(GL13.GL_TEXTURE3);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, idMapaDeNormais);
		GL13.glActiveTexture(GL13.GL_TEXTURE4);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, frameDeBufferDeAgua.getRefractionDepthTexture());
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		
	}
	
	private void unbind(){
		GL11.glDisable(GL11.GL_BLEND);
		GL20.glDisableVertexAttribArray(0);
		GL30.glBindVertexArray(0);
		shader.parar();
	}

	private void setUpVAO(Carregador carregador) {
		// Just x and z vectex positions here, y is set to 0 in v.shader
		float[] vertices = { -1, -1, -1, 1, 1, -1, 1, -1, -1, 1, 1, 1 };
		quad = carregador.carregarAoVAO(vertices,2);
	}

	public FrameDeBufferDeAgua getFrameDeBufferDeAgua() {
		return frameDeBufferDeAgua;
	}

}
