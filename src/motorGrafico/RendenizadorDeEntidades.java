package motorGrafico;

import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.lwjgl.util.vector.Matrix4f;
import org.newdawn.slick.opengl.TextureImpl;

import cenas.Protagonista;
import ferramentas.Calculos;
import modelos.ModeloDeLinha;
import modelos.ModeloTexturizado;
import modelos.ModeloTexturizadoComNormalMap;
import nodes.EntidadeModelada;
import nodes.EntidadeModeladaComNormalMap;
import shaders.ShaderEstatico;
import java.util.Map;
import java.util.List;
public class RendenizadorDeEntidades {


	
	private ShaderEstatico shader;
	
	
	public RendenizadorDeEntidades(ShaderEstatico shader, Matrix4f matrixDeProjecao) {
		this.shader = shader;
	
		shader.comecar();
		shader.carregarMatrixDeProjecao(matrixDeProjecao);
		shader.parar();
	}

	
	
	public void rendenizar(Map<ModeloTexturizado,List<EntidadeModelada>> entidades) {
		for(ModeloTexturizado modelo:entidades.keySet()) {
			prepararModeloTexturizado(modelo);
			List<EntidadeModelada> batch = entidades.get(modelo);
			for(EntidadeModelada entidade:batch) {
				prepararInstancia(entidade);
				GL11.glDrawElements(GL11.GL_TRIANGLES, modelo.getModeloDeLinha().getQuantidadeVertex(), GL11.GL_UNSIGNED_INT, 0);
			}
			desvincularModeloTextura();
		}
	}
	public void rendenizar(Map<ModeloTexturizado,List<EntidadeModelada>> entidades,Map<ModeloTexturizadoComNormalMap,List<EntidadeModeladaComNormalMap>> entidadesNormalMap) {
		if(!entidades.isEmpty()) {
		for(ModeloTexturizado modelo:entidades.keySet()) {
			prepararModeloTexturizado(modelo);
			List<EntidadeModelada> batch = entidades.get(modelo);
			for(EntidadeModelada entidade:batch) {
				prepararInstancia(entidade);
				GL11.glDrawElements(GL11.GL_TRIANGLES, modelo.getModeloDeLinha().getQuantidadeVertex(), GL11.GL_UNSIGNED_INT, 0);
			}
			desvincularModeloTextura();
		}
		}
		if(!entidadesNormalMap.isEmpty()) {
		for(ModeloTexturizadoComNormalMap modelo:entidadesNormalMap.keySet()) {
			prepararModeloTexturizado(modelo);
			List<EntidadeModeladaComNormalMap> batch = entidadesNormalMap.get(modelo);
			for(EntidadeModelada entidade:batch) {
				prepararInstancia(entidade);
				GL11.glDrawElements(GL11.GL_TRIANGLES, modelo.getModeloDeLinha().getQuantidadeVertex(), GL11.GL_UNSIGNED_INT, 0);
			}
			desvincularModeloTextura();
		}
		}
	}
	
	private void prepararModeloTexturizado(ModeloTexturizado modeloTexturizado) {
		ModeloDeLinha modelo = modeloTexturizado.getModeloDeLinha();
		GL30.glBindVertexArray(modelo.getVaoId());
		GL20.glEnableVertexAttribArray(0);
		GL20.glEnableVertexAttribArray(1);
		GL20.glEnableVertexAttribArray(2);
		shader.carregarQuantidadeLinhas(modeloTexturizado.getTextura().getNumeroDeColunas());
		if(modeloTexturizado.getTextura().isTransparencia()) {
			RendenizadorMestre.desativarCulling();
		}
		shader.carregarIluminacaoFalsa(modeloTexturizado.getTextura().isIluminacaoFalsa());
		shader.carregarBrilho(modeloTexturizado.getTextura());
		GL13.glActiveTexture(GL13.GL_TEXTURE0);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, modeloTexturizado.getTextura().getId());
		
	}
	
	private void desvincularModeloTextura() {
		RendenizadorMestre.ativarCulling();
		GL20.glDisableVertexAttribArray(0);
		GL20.glDisableVertexAttribArray(1);
		GL20.glDisableVertexAttribArray(2);
		GL30.glBindVertexArray(0);
	}
	private void prepararInstancia(EntidadeModelada entidade) {
		Matrix4f transformacaoDeMatrix = Calculos.criarTranformacaoMatrix(entidade.getPosicaoGlobal(), entidade.getRotacaoGlobal().x,
				entidade.getRotacaoGlobal().y,  entidade.getRotacaoGlobal().z, entidade.getEscalaGlobal());
		shader.carregarTransformacaoMatrix(transformacaoDeMatrix);
		shader.carregarOffset(entidade.getColuna(), entidade.getLinha());
	}



}
