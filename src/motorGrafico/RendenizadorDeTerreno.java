package motorGrafico;

import shaders.ShaderDeTerreno;
import sombras.ShadowMapMasterRenderer;
import nodes.Terreno;
import texturas.PackDeTexturaDeTerreno;

import java.util.List;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;

import ferramentas.Calculos;
import modelos.ModeloDeLinha;
import modelos.ModeloTexturizado;
import nodes.EntidadeModelada;

public class RendenizadorDeTerreno {
	private ShaderDeTerreno shader;

	public RendenizadorDeTerreno(ShaderDeTerreno shader, Matrix4f matrixDeProjecao) {
		this.shader = shader;
		shader.comecar();
		shader.carregarMatrixDeProjecao(matrixDeProjecao);
		shader.conectarUnidadesDeTextura();
		shader.parar();
	}

	public void rendenizar(List<Terreno> terrenos, ShadowMapMasterRenderer sombras) {
		shader.carregarSombras(sombras.getToShadowMapSpaceMatrix());
		for (Terreno terreno : terrenos) {
			prepararTerreno(terreno,sombras.getShadowMap());
			carregarModeloMatrix(terreno);
			GL11.glDrawElements(GL11.GL_TRIANGLES, terreno.getModelo().getQuantidadeVertex(), GL11.GL_UNSIGNED_INT, 0);
			desvincularModeloTextura();
		}
	}

	private void prepararTerreno(Terreno terreno,int idSombra) {
		ModeloDeLinha modelo = terreno.getModelo();
		GL30.glBindVertexArray(modelo.getVaoId());
		GL20.glEnableVertexAttribArray(0);
		GL20.glEnableVertexAttribArray(1);
		GL20.glEnableVertexAttribArray(2);
		vincularTexturas(terreno,idSombra);
		shader.carregarBrilho(1,0);;
	}

	private void vincularTexturas(Terreno terreno,int idSombra) {
		PackDeTexturaDeTerreno pack = terreno.getPackDeTexturaDeTerro();
		GL13.glActiveTexture(GL13.GL_TEXTURE0);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, pack.getTexturaDeFundo().getId());
		GL13.glActiveTexture(GL13.GL_TEXTURE1);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, pack.getTexturaR().getId());
		GL13.glActiveTexture(GL13.GL_TEXTURE2);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, pack.getTexturaG().getId());
		GL13.glActiveTexture(GL13.GL_TEXTURE3);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, pack.getTexturaB().getId());
		GL13.glActiveTexture(GL13.GL_TEXTURE4);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, terreno.getMapaBlend().getId());
		GL13.glActiveTexture(GL13.GL_TEXTURE5);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, idSombra);
	}

	private void desvincularModeloTextura() {
		GL20.glDisableVertexAttribArray(0);
		GL20.glDisableVertexAttribArray(1);
		GL20.glDisableVertexAttribArray(2);
		GL30.glBindVertexArray(0);
	}

	private void carregarModeloMatrix(Terreno terreno) {
		Matrix4f transformacaoDeMatrix = Calculos
				.criarTranformacaoMatrix(new Vector3f(terreno.getX(), 0, terreno.getZ()), 0, 0, 0, 1);
		shader.carregarTransformacaoMatrix(transformacaoDeMatrix);
	}
}
