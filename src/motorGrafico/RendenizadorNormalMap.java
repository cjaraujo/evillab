package motorGrafico;

import java.util.List;
import java.util.Map;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector4f;

import ferramentas.Calculos;
import modelos.ModeloDeLinha;
import modelos.ModeloTexturizado;
import modelos.ModeloTexturizadoComNormalMap;
import nodes.Camera;
import nodes.EntidadeModeladaComNormalMap;
import nodes.Luz;

import shaders.NormalMappingShader;
import texturas.TexturaDeModelo;


public class RendenizadorNormalMap {

	private NormalMappingShader shader;

	public RendenizadorNormalMap(Matrix4f projectionMatrix) {
		this.shader = new NormalMappingShader();
		shader.comecar();
		shader.loadProjectionMatrix(projectionMatrix);
		shader.connectTextureUnits();
		shader.parar();
	}

	public void render(Map<ModeloTexturizadoComNormalMap, List<EntidadeModeladaComNormalMap>> entities, Vector4f clipPlane, List<Luz> lights, Camera camera) {
		shader.comecar();
		prepare(clipPlane, lights, camera);
		for (ModeloTexturizadoComNormalMap model : entities.keySet()) {
			prepareTexturedModel(model);
			List<EntidadeModeladaComNormalMap> batch = entities.get(model);
			for (EntidadeModeladaComNormalMap entity : batch) {
				prepareInstance(entity);
				GL11.glDrawElements(GL11.GL_TRIANGLES, model.getModeloDeLinha().getQuantidadeVertex(), GL11.GL_UNSIGNED_INT, 0);
			}
			unbindTexturedModel();
		}
		shader.parar();
	}
	
	public void limpar(){
		shader.limpar();
	}

	private void prepareTexturedModel(ModeloTexturizadoComNormalMap model) {
		ModeloDeLinha rawModel = model.getModeloDeLinha();
		GL30.glBindVertexArray(rawModel.getVaoId());
		GL20.glEnableVertexAttribArray(0);
		GL20.glEnableVertexAttribArray(1);
		GL20.glEnableVertexAttribArray(2);
		GL20.glEnableVertexAttribArray(3);
		TexturaDeModelo texture = model.getTextura();
		shader.loadNumberOfRows(texture.getNumeroDeColunas());
		if (texture.isIluminacaoFalsa()) {
			RendenizadorMestre.desativarCulling();;
		}
		shader.loadShineVariables(texture.getBrilho(), texture.getReflexibilidade());
		GL13.glActiveTexture(GL13.GL_TEXTURE0);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, model.getTextura().getId());
		GL13.glActiveTexture(GL13.GL_TEXTURE1);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D,  model.getTextura().getNormalMap());
	}

	private void unbindTexturedModel() {
		RendenizadorMestre.ativarCulling();
		GL20.glDisableVertexAttribArray(0);
		GL20.glDisableVertexAttribArray(1);
		GL20.glDisableVertexAttribArray(2);
		GL20.glDisableVertexAttribArray(3);
		GL30.glBindVertexArray(0);
	}

	private void prepareInstance(EntidadeModeladaComNormalMap entity) {
		Matrix4f transformationMatrix = Calculos.criarTranformacaoMatrix(entity.getPosicaoGlobal(), entity.getRotacaoGlobal().x,
				entity.getRotacaoGlobal().y, entity.getRotacaoGlobal().z, entity.getEscalaGlobal());
		shader.loadTransformationMatrix(transformationMatrix);
		shader.loadOffset(entity.getColuna(), entity.getLinha());
	}

	private void prepare(Vector4f clipPlane, List<Luz> lights, Camera camera) {
		shader.loadClipPlane(clipPlane);
		//need to be public variables in MasterRenderer
		shader.loadSkyColour(RendenizadorMestre.RED, RendenizadorMestre.GREEN, RendenizadorMestre.BLUE);
		Matrix4f viewMatrix = Calculos.criarMatrixDeVisao(camera);
		
		shader.loadLights(lights, viewMatrix);
		shader.loadViewMatrix(viewMatrix);
	}

}
