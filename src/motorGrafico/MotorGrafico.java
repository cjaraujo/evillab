package motorGrafico;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL30;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;

import agua.FrameDeBufferDeAgua;
import modelos.ModeloTexturizado;
import nodes.Camera;
import nodes.EntidadeModelada;
import nodes.Gui;
import nodes.Luz;
import nodes.Node;
import nodes.Raiz;
import nodes.Terreno;
import sombras.ShadowMapMasterRenderer;
import texturas.Textura;

public class MotorGrafico {
	private Carregador carregador;

	private RendenizadorMestre rendenizador;

	private RendenizadorDeGui rendenizadorDeGui;
	private RendenizadorDeAgua rendenizadorDeAgua;
	private FrameDeBufferDeAgua frameDeBufferDeAgua;
	private Gui gui;
	private Gui gui2;
	private List<Gui> guis = new ArrayList<Gui>();
	private ShadowMapMasterRenderer shadowMapMasterRenderer;
	
	public MotorGrafico(Carregador carregador) {
		frameDeBufferDeAgua = new FrameDeBufferDeAgua();
		shadowMapMasterRenderer = new ShadowMapMasterRenderer();
		rendenizador = new RendenizadorMestre(carregador,frameDeBufferDeAgua);
		rendenizador.preparar();
		rendenizadorDeGui = new RendenizadorDeGui(carregador);
		
//		gui = new Gui("gui", "gui", new Raiz(), new Vector2f(0.5f,-0.5f), new Vector2f(0.5f,0.5f), new Textura(shadowMapMasterRenderer.getShadowMap()));
//		this.guis.add(gui);
	}

	public void rendenizar(Raiz raiz, Camera camera) {

		shadowMapMasterRenderer.render(raiz.getFilhosParaRendenizar(), (Luz) raiz.getFilhos().get("luz4"), camera);
		
		GL11.glEnable(GL30.GL_CLIP_DISTANCE0);
		frameDeBufferDeAgua.bindReflectionFrameBuffer();
		float distancia = 2* (camera.getPosicaoGlobal().y-raiz.getAguas().get(0).getPosicao().y);
		camera.getPosicaoGlobal().y -= distancia;
		camera.getRotacao().x = -camera.getRotacao().x;
		rendenizador.rendenizar(raiz.getLuzes(),raiz.getTerrenos(),raiz.getFilhosParaRendenizar(),raiz.getFilhosParaRendenizarComNormalMap(), camera,new Vector4f(0,1,0,-raiz.getAguas().get(0).getPosicao().y-1f),shadowMapMasterRenderer);
		camera.getPosicaoGlobal().y += distancia;
		camera.getRotacao().x = -camera.getRotacao().x;
		frameDeBufferDeAgua.bindRefractionFrameBuffer();
		GL11.glEnable(GL30.GL_CLIP_DISTANCE0);
		rendenizador.rendenizar(raiz.getLuzes(),raiz.getTerrenos(),raiz.getFilhosParaRendenizar(),raiz.getFilhosParaRendenizarComNormalMap(), camera,new Vector4f(0,-1,0,raiz.getAguas().get(0).getPosicao().y+1f),shadowMapMasterRenderer);
		frameDeBufferDeAgua.unbindCurrentFrameBuffer();
		GL11.glDisable(GL30.GL_CLIP_DISTANCE0);
		rendenizador.rendenizar(raiz.getLuzes(),raiz.getTerrenos(),raiz.getFilhosParaRendenizar(),raiz.getFilhosParaRendenizarComNormalMap(), camera,new Vector4f(0,0,0,0),shadowMapMasterRenderer);
		rendenizador.getRendenizadorDeAgua().render(raiz.getAguas(), camera,raiz.getLuzes());
		rendenizadorDeGui.rendenizar(guis);
	}
	


	public RendenizadorMestre getRendenizador() {
		return rendenizador;
	}

	public Carregador getCarregador() {
		return carregador;
	}

	public void limpar() {
		frameDeBufferDeAgua.cleanUp();
		rendenizador.limpar();
	}

}
