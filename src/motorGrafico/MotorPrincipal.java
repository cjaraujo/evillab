package motorGrafico;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.util.vector.Vector3f;

import cenas.CenaPrincipal;
import ferramentas.VetorMouse;
import modelos.ModeloDeLinha;
import modelos.ModeloTexturizado;
import nodes.Camera;
import nodes.EntidadeComplexa;
import nodes.EntidadeModelada;
import nodes.Luz;
import nodes.Node;
import nodes.Raiz;
import shaders.ShaderEstatico;
import sombras.ShadowMapMasterRenderer;
import texturas.TexturaDeModelo;
import texturas.PackDeTexturaDeTerreno;
import texturas.Textura;

public class MotorPrincipal {
	private Carregador carregador;
	
	private Camera cameraPrincipal;
	private MotorGrafico motorGrafico;
	public MotorPrincipal() {
		GerenciadorDeTela.criarTela();
		
		
		carregador = new Carregador();
		motorGrafico = new MotorGrafico(carregador);
		
	}
	public void iniciar() {
		
		
		Raiz raiz = new Raiz(this);
		CenaPrincipal cenaPrincipal = new CenaPrincipal("CENA_PRINCIPAL", "Cena Principal", raiz);
		raiz.adicionarFilho(cenaPrincipal);
		raiz.setNodePrincipal(cenaPrincipal);
		
		while (!Display.isCloseRequested()) {

			raiz.processar(GerenciadorDeTela.obterDelta());
			motorGrafico.rendenizar(raiz,cameraPrincipal);
			GerenciadorDeTela.atualizarTela();
		}
		motorGrafico.limpar();
		carregador.limpar();
		
		GerenciadorDeTela.fecharTela();
	}
	
	public MotorGrafico getMotorGrafico() {
		return motorGrafico;
	}
	public Camera getCameraPrincipal() {
		return cameraPrincipal;
	}

	public void setCameraPrincipal(Camera cameraPrincipal) {
		this.cameraPrincipal = cameraPrincipal;
	}


	public Carregador getCarregador() {
		return carregador;
	}

}
