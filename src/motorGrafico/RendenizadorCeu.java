package motorGrafico;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;

import modelos.ModeloDeLinha;
import nodes.Camera;
import shaders.SkyboxShader;

public class RendenizadorCeu {
	private static final float TAMANHO = 1000f;

	private static final float[] VERTICES = { -TAMANHO, TAMANHO, -TAMANHO, -TAMANHO, -TAMANHO, -TAMANHO, TAMANHO,
			-TAMANHO, -TAMANHO, TAMANHO, -TAMANHO, -TAMANHO, TAMANHO, TAMANHO, -TAMANHO, -TAMANHO, TAMANHO, -TAMANHO,

			-TAMANHO, -TAMANHO, TAMANHO, -TAMANHO, -TAMANHO, -TAMANHO, -TAMANHO, TAMANHO, -TAMANHO, -TAMANHO, TAMANHO,
			-TAMANHO, -TAMANHO, TAMANHO, TAMANHO, -TAMANHO, -TAMANHO, TAMANHO,

			TAMANHO, -TAMANHO, -TAMANHO, TAMANHO, -TAMANHO, TAMANHO, TAMANHO, TAMANHO, TAMANHO, TAMANHO, TAMANHO,
			TAMANHO, TAMANHO, TAMANHO, -TAMANHO, TAMANHO, -TAMANHO, -TAMANHO,

			-TAMANHO, -TAMANHO, TAMANHO, -TAMANHO, TAMANHO, TAMANHO, TAMANHO, TAMANHO, TAMANHO, TAMANHO, TAMANHO,
			TAMANHO, TAMANHO, -TAMANHO, TAMANHO, -TAMANHO, -TAMANHO, TAMANHO,

			-TAMANHO, TAMANHO, -TAMANHO, TAMANHO, TAMANHO, -TAMANHO, TAMANHO, TAMANHO, TAMANHO, TAMANHO, TAMANHO,
			TAMANHO, -TAMANHO, TAMANHO, TAMANHO, -TAMANHO, TAMANHO, -TAMANHO,

			-TAMANHO, -TAMANHO, -TAMANHO, -TAMANHO, -TAMANHO, TAMANHO, TAMANHO, -TAMANHO, -TAMANHO, TAMANHO, -TAMANHO,
			-TAMANHO, -TAMANHO, -TAMANHO, TAMANHO, TAMANHO, -TAMANHO, TAMANHO };
	private static String[] texturas = { "right", "left", "top","bottom","back","front" };
	private ModeloDeLinha modelo;
	private int textura;
	private SkyboxShader shader;
	public RendenizadorCeu(Carregador carregador, Matrix4f matrixProjecao) {
		modelo = carregador.carregarAoVAO(VERTICES,3);
		textura = carregador.carregarMapaDeCubo(texturas);
		shader = new SkyboxShader();
		shader.comecar();
		shader.loadProjectionMatrix(matrixProjecao);
		shader.parar();
	}
	public void rendenizar(Camera camera,Vector4f plano) {
		shader.comecar();
		shader.loadViewMatrix(camera);
		shader.carregarCorDoCeu(new Vector3f(RendenizadorMestre.RED,RendenizadorMestre.GREEN,RendenizadorMestre.BLUE));
		shader.carregarPlano(plano);
		GL30.glBindVertexArray(modelo.getVaoId());
		GL20.glEnableVertexAttribArray(0);
		GL13.glActiveTexture(GL13.GL_TEXTURE0);
		GL11.glBindTexture(GL13.GL_TEXTURE_CUBE_MAP, textura);
		GL11.glDrawArrays(GL11.GL_TRIANGLES, 0, modelo.getQuantidadeVertex());
		GL20.glDisableVertexAttribArray(0);
		GL30.glBindVertexArray(0);
		shader.parar();
	}
}
