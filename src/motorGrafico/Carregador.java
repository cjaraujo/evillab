package motorGrafico;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL14;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import de.matthiasmann.twl.utils.PNGDecoder;
import de.matthiasmann.twl.utils.PNGDecoder.Format;
import modelos.ModeloDeLinha;
import modelos.TextureData;

public class Carregador {
	private List<Integer> vaos = new ArrayList<Integer>();
	private List<Integer> vbos = new ArrayList<Integer>();
	private List<Integer> texturas = new ArrayList<Integer>();

	public ModeloDeLinha carregarAoVAO(float[] posicoes, float[] cordenadasDeTextura, float[] normais, int[] indices) {
		int vaoId = criarVAO();
		ligarIndicesBuffer(indices);
		vaos.add(vaoId);
		salvarDadosNaListaDeAtributos(0, 3, posicoes);
		salvarDadosNaListaDeAtributos(1, 2, cordenadasDeTextura);
		salvarDadosNaListaDeAtributos(2, 3, normais);
		desvincularVAO();
		return new ModeloDeLinha(vaoId, indices.length);
	}
	private void salvarDadosNaListaDeAtributos(int numeroDoAtributo, int quantidadeCordenadas, int[] jointIds) {
		// TODO Auto-generated method stub
		
	}
	public ModeloDeLinha carregarAoVAO(float[] posicoes, float[] cordenadasDeTextura, float[] normais,
			float[] tangentes, int[] indices) {
		int vaoId = criarVAO();
		ligarIndicesBuffer(indices);
		vaos.add(vaoId);
		salvarDadosNaListaDeAtributos(0, 3, posicoes);
		salvarDadosNaListaDeAtributos(1, 2, cordenadasDeTextura);
		salvarDadosNaListaDeAtributos(2, 3, normais);
		salvarDadosNaListaDeAtributos(3, 3, tangentes);
		desvincularVAO();
		return new ModeloDeLinha(vaoId, indices.length);
	}

	public ModeloDeLinha carregarAoVAO(float[] posicoes) {
		int vaoId = criarVAO();
		vaos.add(vaoId);
		salvarDadosNaListaDeAtributos(0, 2, posicoes);
		desvincularVAO();
		return new ModeloDeLinha(vaoId, posicoes.length);
	}

	public ModeloDeLinha carregarAoVAO(float[] positions, int dimensions) {
		int vaoID = criarVAO();
		salvarDadosNaListaDeAtributos(0, dimensions, positions);
		desvincularVAO();
		return new ModeloDeLinha(vaoID, positions.length / dimensions);
	}

	public void limpar() {
		for (int vao : vaos) {
			GL30.glDeleteVertexArrays(vao);
		}
		for (int vbo : vbos) {
			GL15.glDeleteBuffers(vbo);
		}
		for (int textura : texturas) {
			GL11.glDeleteTextures(textura);
		}
	}

	private void ligarIndicesBuffer(int[] indices) {
		int vboId = GL15.glGenBuffers();

		vbos.add(vboId);
		GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, vboId);
		IntBuffer buffer = salvarDadosNoIntBuffer(indices);
		GL15.glBufferData(GL15.GL_ELEMENT_ARRAY_BUFFER, buffer, GL15.GL_STATIC_DRAW);

	}

	private IntBuffer salvarDadosNoIntBuffer(int[] dados) {
		IntBuffer buffer = BufferUtils.createIntBuffer(dados.length);
		buffer.put(dados);
		buffer.flip();
		return buffer;
	}

	private TextureData decodificarArquivoDeTextura(String fileName) {
		int width = 0;
		int height = 0;
		ByteBuffer buffer = null;
		try {
			FileInputStream in = new FileInputStream(fileName);
			PNGDecoder decoder = new PNGDecoder(in);
			width = decoder.getWidth();
			height = decoder.getHeight();
			buffer = ByteBuffer.allocateDirect(4 * width * height);
			decoder.decode(buffer, width * 4, Format.RGBA);
			buffer.flip();
			in.close();
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("Tried to load texture " + fileName + ", didn't work");
			System.exit(-1);
		}
		return new TextureData(buffer, width, height);
	}

	public int criarVAO() {
		int vaoId = GL30.glGenVertexArrays();
		GL30.glBindVertexArray(vaoId);
		return vaoId;
	}

	public int carregarMapaDeCubo(String[] texturas) {
		int idTextura = GL11.glGenTextures();
		GL13.glActiveTexture(GL13.GL_TEXTURE0);
		GL11.glBindTexture(GL13.GL_TEXTURE_CUBE_MAP, idTextura);
		for(int i=0;i<texturas.length;i++) {
			TextureData textureData = decodificarArquivoDeTextura("res/"+texturas[i]+".png");
			GL11.glTexImage2D(GL13.GL_TEXTURE_CUBE_MAP_POSITIVE_X+i, 0, GL11.GL_RGBA, textureData.getWidth(), textureData.getHeight(), 0, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, textureData.getBuffer());
		}
		GL11.glTexParameteri(GL13.GL_TEXTURE_CUBE_MAP, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);
		GL11.glTexParameteri(GL13.GL_TEXTURE_CUBE_MAP, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR);
		this.texturas.add(idTextura);
		return idTextura;
	}

	private void salvarDadosNaListaDeAtributos(int numeroDoAtributo, int quantidadeCordenadas, float[] dados) {
		int vboId = GL15.glGenBuffers();
		vbos.add(vboId);
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vboId);
		FloatBuffer buffer = salvarDadosNoFloatBuffer(dados);
		GL15.glBufferData(GL15.GL_ARRAY_BUFFER, buffer, GL15.GL_STATIC_DRAW);
		GL20.glVertexAttribPointer(numeroDoAtributo, quantidadeCordenadas, GL11.GL_FLOAT, false, 0, 0);
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);
	}

	public int carregarTextura(String nomeDoArquivo) {
		Texture textura = null;
		try {
			textura = TextureLoader.getTexture("PNG", new FileInputStream("res/" + nomeDoArquivo + ".png"));
			GL30.glGenerateMipmap(GL11.GL_TEXTURE_2D);
			GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR_MIPMAP_LINEAR);
			GL11.glTexParameterf(GL11.GL_TEXTURE_2D, GL14.GL_TEXTURE_LOD_BIAS, -0.3f);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		int texturaId = textura.getTextureID();
		texturas.add(texturaId);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_S, GL11.GL_REPEAT);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_T, GL11.GL_REPEAT);
		return texturaId;

	}

	public void desvincularVAO() {
		GL30.glBindVertexArray(0);
	}

	private FloatBuffer salvarDadosNoFloatBuffer(float[] dados) {
		FloatBuffer buffer = BufferUtils.createFloatBuffer(dados.length);
		buffer.put(dados);
		buffer.flip();
		return buffer;
	}

}
