package motorGrafico;

import org.lwjgl.LWJGLException;
import org.lwjgl.Sys;
import org.lwjgl.opengl.ContextAttribs;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.PixelFormat;

public class GerenciadorDeTela {
	private static final int WIDTH = 1280;
	private static final int HEIGHT = 720;
	private static final int FPS = 120;
	
	private static long ultimoFrame;
	private static float delta;
	

	public static void criarTela() {
		ContextAttribs attribs = new ContextAttribs(3, 2);
		attribs.withForwardCompatible(true);
		attribs.withProfileCore(true);
		try {
			Display.setDisplayMode(new DisplayMode(WIDTH, HEIGHT));
			Display.create(new PixelFormat(), attribs);
			Display.setTitle("Singularidade");
		} catch (LWJGLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		GL11.glViewport(0, 0, WIDTH, HEIGHT);
		ultimoFrame = obterTempoAtual();
	}

	public static void atualizarTela() {
		Display.sync(FPS);
		Display.update();
		long frameAtual = obterTempoAtual();
		delta = (frameAtual - ultimoFrame)/1000f;
		ultimoFrame = frameAtual;
	}
	public static float obterDelta() {
		return delta;
	}

	public static void fecharTela() {
		Display.destroy();
	}
	private static long obterTempoAtual() {
		return Sys.getTime()*1000/Sys.getTimerResolution();
	}
}
