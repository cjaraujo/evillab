package ferramentas;

import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

import nodes.Camera;

public class Calculos {
	public static Matrix4f criarTranformacaoMatrix(Vector3f translacao, float rx, float ry, float rz, float scale) {
		Matrix4f matrix = new Matrix4f();
		matrix.setIdentity();
		Matrix4f.translate(translacao, matrix, matrix);
		Matrix4f.rotate((float) Math.toRadians(rx), new Vector3f(1, 0, 0), matrix, matrix);
		Matrix4f.rotate((float) Math.toRadians(ry), new Vector3f(0, 1, 0), matrix, matrix);
		Matrix4f.rotate((float) Math.toRadians(rz), new Vector3f(0, 0, 1), matrix, matrix);
		Matrix4f.scale(new Vector3f(scale, scale, scale), matrix, matrix);
		return matrix;

	}
	public static Matrix4f criarMatrixDeVisao(Camera camera) {
		Matrix4f matrixDeVisao = new Matrix4f();
		matrixDeVisao.setIdentity();
		Matrix4f.rotate((float) Math.toRadians(camera.getRotacao().x), new Vector3f(1,0,0), matrixDeVisao, matrixDeVisao);
		Matrix4f.rotate((float) Math.toRadians(camera.getRotacao().y), new Vector3f(0,1,0), matrixDeVisao, matrixDeVisao);
		Matrix4f.rotate((float) Math.toRadians(camera.getRotacao().z), new Vector3f(0,0,1), matrixDeVisao, matrixDeVisao);
		Vector3f posicaoCamera = camera.getPosicaoGlobal();
		Vector3f posicaoNegativaCamera = new Vector3f(-posicaoCamera.x,-posicaoCamera.y,-posicaoCamera.z);
		Matrix4f.translate(posicaoNegativaCamera, matrixDeVisao, matrixDeVisao);
		return matrixDeVisao;
	}
	public static float bariCentro(Vector3f p1, Vector3f p2, Vector3f p3, Vector2f pos) {
		float det = (p2.z - p3.z) * (p1.x - p3.x) + (p3.x - p2.x) * (p1.z - p3.z);
		float l1 = ((p2.z - p3.z) * (pos.x - p3.x) + (p3.x - p2.x) * (pos.y - p3.z)) / det;
		float l2 = ((p3.z - p1.z) * (pos.x - p3.x) + (p1.x - p3.x) * (pos.y - p3.z)) / det;
		float l3 = 1.0f - l1 - l2;
		return l1 * p1.y + l2 * p2.y + l3 * p3.y;
	}
	public static Matrix4f criarTranformacaoMatrix(Vector2f translation, Vector2f scale) {
		Matrix4f matrix = new Matrix4f();
		matrix.setIdentity();
		Matrix4f.translate(translation, matrix, matrix);
		Matrix4f.scale(new Vector3f(scale.x, scale.y, 1f), matrix, matrix);
		return matrix;
	}
	
}
