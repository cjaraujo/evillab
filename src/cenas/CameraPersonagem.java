package cenas;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.util.vector.Vector3f;

import nodes.Camera;
import nodes.Node;
import nodes.Raiz;

public class CameraPersonagem extends Camera {
	private float ALTURA_PERSONAGEM = 70f;
	private static float ROTACAO_MAXIMA = 90;
	private static float ROTACAO_MINIMA = 10;
	private float distancia = 50;
	private Protagonista protagonista;

	public CameraPersonagem(String chave, String nome, Raiz pai, boolean ativo, Vector3f posicao,
			Protagonista protagonista) {
		super(chave, nome, pai, ativo, posicao);
		this.protagonista = protagonista;
	}

	public void processar(float delta) {
		if (Mouse.getY() > Display.getHeight() / 2) {
			getRotacao().x -= 150f * delta;
		} else if (Mouse.getY() < Display.getHeight() / 2) {
			getRotacao().x += 150f * delta;
		}
		if(getRotacao().x>ROTACAO_MAXIMA) {
			getRotacao().x = ROTACAO_MAXIMA;
		}else if(getRotacao().x<ROTACAO_MINIMA) {
			getRotacao().x = ROTACAO_MINIMA;
		}
		float variacao = Mouse.getDWheel() * delta;

		distancia -= variacao;
		
		getRotacao().y = protagonista.getAnguloDeGiro();
		
		float distanciaChao = (float) ((distancia) * Math.cos(Math.toRadians(getRotacao().x)));
		float novoZ = (float) (distanciaChao * Math.cos(Math.toRadians(getRotacao().y)));
		float novoX = -(float) ((distanciaChao) * Math.sin(Math.toRadians(getRotacao().y)));
		float novoY = (float) ((distancia) * Math.sin(Math.toRadians(getRotacao().x)))+5;
		float alturaChao = protagonista.getTerreno().getAlturaDoTerreno(protagonista.getPosicao().x + novoX, protagonista.getPosicao().z + novoZ);
		if(novoY<=alturaChao) {
			novoY = alturaChao;
		}
		setPosicao(new Vector3f(protagonista.getPosicao().x + novoX,
				protagonista.getPosicao().y + novoY, protagonista.getPosicao().z + novoZ));

	}

}
