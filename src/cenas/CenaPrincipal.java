package cenas;

import java.util.Random;

import org.lwjgl.util.vector.Vector;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

import modelos.ModeloDeLinha;
import modelos.ModeloTexturizado;
import modelos.ModeloTexturizadoComNormalMap;
import motorGrafico.CarregadorObj;
import motorGrafico.MotorPrincipal;
import motorGrafico.NormalMappedObjLoader;
import nodes.Agua;
import nodes.Camera;
import nodes.EntidadeModelada;
import nodes.EntidadeModeladaComNormalMap;
import nodes.Gui;
import nodes.Luz;
import nodes.LuzLocal;
import nodes.Node;
import nodes.Raiz;
import nodes.Terreno;
import texturas.TexturaDeModelo;
import texturas.TexturaDeModeloComNormalMap;
import texturas.PackDeTexturaDeTerreno;
import texturas.Textura;

public class CenaPrincipal extends Node {
	private EntidadeModeladaComNormalMap entidadeModeladanormalmap;
	public CenaPrincipal(String chave, String nome, Raiz pai) {
		super(chave, nome, pai);

	}
	
	public void preparar() {
		Textura texturaDeFundo = new Textura(
				this.getMotorPrincipal().getCarregador().carregarTextura("grass"));
		Textura texturaR = new Textura(
				this.getMotorPrincipal().getCarregador().carregarTextura("path"));
		Textura texturaB = new Textura(
				this.getMotorPrincipal().getCarregador().carregarTextura("mud"));
		Textura texturaG = new Textura(
				this.getMotorPrincipal().getCarregador().carregarTextura("grassy2"));
		Textura mapaBlend = new Textura(
				this.getMotorPrincipal().getCarregador().carregarTextura("blendMap"));
		PackDeTexturaDeTerreno pack = new PackDeTexturaDeTerreno(texturaDeFundo, texturaR, texturaB, texturaG);
		Terreno terreno = new Terreno("terreno", "terreno", this, true, 0, 0, pack, mapaBlend, "heightmap");
		this.adicionarFilho(terreno);

		TexturaDeModelo modelo = new TexturaDeModelo(
				this.getMotorPrincipal().getCarregador().carregarTextura("playerTexture"));
		ModeloTexturizado modeloTexturizado = new ModeloTexturizado(
				CarregadorObj.carregarModeloObj("person", this.getMotorPrincipal().getCarregador()), modelo);
		Protagonista entidade = new Protagonista("cleomar", "cleomar", this, true, new Vector3f(0, 0, 0),
				new Vector3f(0, 0, 0), modeloTexturizado, 1f, terreno);
		adicionarFilho(entidade);
		Random random = new Random(253263);
		TexturaDeModelo modelo1 = new TexturaDeModelo(
				this.getMotorPrincipal().getCarregador().carregarTextura("fern"));
		ModeloTexturizado modeloTexturizado1 = new ModeloTexturizado(
				CarregadorObj.carregarModeloObj("fern", this.getMotorPrincipal().getCarregador()), modelo1);
		modelo1.setNumeroDeColunas(2);
		modelo1.setTransparencia(true);
		for (int i = 0; i < 30; i++) {
			float x = random.nextFloat()*400;
			float z = random.nextFloat()*400;
			float y = terreno.getAlturaDoTerreno(x, z);
	
			
			
			EntidadeModelada entidade1 = new EntidadeModelada("fern"+i, "fern"+i, this, true, new Vector3f(x, y, z),
					new Vector3f(0, 0, 0), modeloTexturizado1, 1, random.nextInt(4));
			adicionarFilho(entidade1);
		}
		TexturaDeModelo modelo2 = new TexturaDeModelo(
				this.getMotorPrincipal().getCarregador().carregarTextura("tree"));
		ModeloTexturizado modeloTexturizado2 = new ModeloTexturizado(
				CarregadorObj.carregarModeloObj("tree", this.getMotorPrincipal().getCarregador()), modelo2);
		modelo2.setNumeroDeColunas(1);
		for (int i = 0; i < 30; i++) {
			float x = random.nextFloat()*400;
			float z = random.nextFloat()*400;
			float y = terreno.getAlturaDoTerreno(x, z);
		
			
			
			EntidadeModelada entidade1 = new EntidadeModelada("tree"+i, "tree"+i, this, true, new Vector3f(x, y, z),
					new Vector3f(0, 0, 0), modeloTexturizado2,20, 0);
			adicionarFilho(entidade1);
		}
//		Textura textura = new Textura(getMotorPrincipal().getCarregador().carregarTextura("grass"));
//		Gui gui =new Gui("gui", "gui", this, new Vector2f(-0.5f,0.5f), new Vector2f(0.5f,0.5f), textura);
//		adicionarFilho(gui);
		TexturaDeModelo texturalampada = new TexturaDeModelo(
				this.getMotorPrincipal().getCarregador().carregarTextura("lamp"));
		texturalampada.setIluminacaoFalsa(true);
		ModeloTexturizado modeloLamopada = new ModeloTexturizado(
				CarregadorObj.carregarModeloObj("lamp", this.getMotorPrincipal().getCarregador()), texturalampada);
		EntidadeModelada entidadeLampada = new EntidadeModelada("lampada", "lampada", this, true, new Vector3f(380, terreno.getAlturaDoTerreno(380, 190),190),
				new Vector3f(0, 0, 0), modeloLamopada, 1, random.nextInt(4));
		
		adicionarFilho(entidadeLampada);
		LuzLocal luz = new LuzLocal("luz", "luz", entidadeLampada, new Vector3f(0,13,0), new Vector3f(1,1,1),new Vector3f(0.9f,0.001f,0.0002f),3);
		entidadeLampada.adicionarFilho(luz);
		
		
		EntidadeModelada entidadeLampada2 = new EntidadeModelada("lampada2", "lampada2", this, true, new Vector3f(480, terreno.getAlturaDoTerreno(480, 300),300),
				new Vector3f(0, 0, 0), modeloLamopada, 1, random.nextInt(4));
		
		adicionarFilho(entidadeLampada2);
		LuzLocal luz2 = new LuzLocal("luz2", "luz2", entidadeLampada2, new Vector3f(0,13,0), new Vector3f(1,1,1),new Vector3f(0.9f,0.001f,0.0002f),3);
		entidadeLampada2.adicionarFilho(luz2);
		
		Luz luz4 = new Luz("luz4", "luz4", this, new Vector3f(-600000,600000,0), new Vector3f(1,1,1),0.2f);
		adicionarFilho(luz4);
		Agua agua = new Agua("agua", "teaguaste", this, new Vector3f(380,-15,400), 200);
		adicionarFilho(agua);
		TexturaDeModeloComNormalMap texturaDeModeloComNormalMap = new TexturaDeModeloComNormalMap(getMotorPrincipal().getCarregador().carregarTextura("barrel"),getMotorPrincipal().getCarregador().carregarTextura("barrelNormal"));
		texturaDeModeloComNormalMap.setBrilho(1);
		texturaDeModeloComNormalMap.setReflexibilidade(0.2f);
		ModeloTexturizadoComNormalMap normalmap = new ModeloTexturizadoComNormalMap(NormalMappedObjLoader.loadOBJ("barrel", getMotorPrincipal().getCarregador()), texturaDeModeloComNormalMap);
		entidadeModeladanormalmap = new EntidadeModeladaComNormalMap("normalmap", "normalmap", this, true, new Vector3f(450,10,220),  new Vector3f(0,0,0), normalmap, 1);
		adicionarFilho(entidadeModeladanormalmap);
	}
	public void processar(float delta) {
		entidadeModeladanormalmap.mudarRotacao(0, delta* 100, 0);
	}

}
