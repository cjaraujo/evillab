package cenas;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.util.vector.Vector3f;

import modelos.ModeloTexturizado;
import nodes.EntidadeComplexa;
import nodes.EntidadeModelada;
import nodes.Raiz;
import nodes.Terreno;

public class Protagonista extends EntidadeModelada {
	private final static float VELOCIDADE_ANGULAR = 300;
	private static float VELOCIDADE_LINEAR = 20;
	private float anguloDeGiro;
	private float velocidadeAtual;
	private float anguloAtual = 0;
	private float sensibilidadeDoMouse = 300f;
	private Terreno terreno;
	public Protagonista(String chave, String nome, Raiz pai, boolean ativo, Vector3f posicao, Vector3f rotacao,
			ModeloTexturizado modelo, float escala,Terreno terreno) {
		super(chave, nome, pai, ativo, posicao, rotacao,modelo, escala);
		this.terreno = terreno;
	}
	public void preparar() {
		CameraPersonagem camera = new CameraPersonagem("protagonista.camera", "Camera", getPai(), true, new Vector3f(0,100,-23),this);
		camera.getRotacao().x =30;
		getPai().adicionarFilho(camera);
		getMotorPrincipal().setCameraPrincipal(camera);
	}

	public void processar(float delta) {
		if(Keyboard.isKeyDown(Keyboard.KEY_LSHIFT)) {
			VELOCIDADE_LINEAR = 20;
		 }else {
			 VELOCIDADE_LINEAR = 10;
		 }
		if (Keyboard.isKeyDown(Keyboard.KEY_W)) {
			 carregarAngulo(delta);
			 
			super.mudarPosicao(VELOCIDADE_LINEAR * (float) (Math.sin(Math.toRadians(anguloAtual))) * delta, 0,
					-delta * VELOCIDADE_LINEAR * (float) (Math.cos(Math.toRadians(anguloAtual))));

		} else if (Keyboard.isKeyDown(Keyboard.KEY_S)) {
			carregarAngulo(delta);
			super.mudarPosicao(-VELOCIDADE_LINEAR * (float) (Math.sin(Math.toRadians(anguloAtual))) * delta, 0,
					delta * VELOCIDADE_LINEAR * (float) (Math.cos(Math.toRadians(anguloAtual))));
		}
		if(Keyboard.isKeyDown(Keyboard.KEY_D)) {
			carregarAngulo(delta);
			super.mudarPosicao(VELOCIDADE_LINEAR * (float) (Math.cos(Math.toRadians(anguloAtual))) * delta, 0,
					delta * VELOCIDADE_LINEAR * (float) (Math.sin(Math.toRadians(anguloAtual))));
		}else if(Keyboard.isKeyDown(Keyboard.KEY_A)) {
			carregarAngulo(delta);
			super.mudarPosicao(-VELOCIDADE_LINEAR * (float) (Math.cos(Math.toRadians(anguloAtual))) * delta, 0,
					-delta * VELOCIDADE_LINEAR * (float) (Math.sin(Math.toRadians(anguloAtual))));
		}
		if (Mouse.getX() > Display.getWidth() / 2) {
			anguloDeGiro += sensibilidadeDoMouse * delta;
		} else if (Mouse.getX() < Display.getWidth() / 2) {
			anguloDeGiro -= sensibilidadeDoMouse * delta;
		}
		if (anguloDeGiro < 0) {
			anguloDeGiro += 360 + anguloDeGiro;
		}
		anguloDeGiro = anguloDeGiro % 360;
		
		getPosicao().y = terreno.getAlturaDoTerreno(getPosicao().x, getPosicao().z);


	}
	
	public Terreno getTerreno() {
		return terreno;
	}
	private void carregarAngulo(float delta) {
		if (anguloAtual > anguloDeGiro) {

			if (anguloAtual - anguloDeGiro > 180) {
				anguloAtual += VELOCIDADE_ANGULAR * delta;
			} else {
				anguloAtual -= VELOCIDADE_ANGULAR * delta;
			}

		} else if (anguloAtual < anguloDeGiro) {
			if (anguloDeGiro - anguloAtual > 180) {
				anguloAtual -= VELOCIDADE_ANGULAR * delta;
			} else {
				anguloAtual += VELOCIDADE_ANGULAR * delta;
			}

		}
		if (anguloAtual > 360) {
			anguloAtual = anguloAtual % 360;
		} else if (anguloAtual < 0) {
			anguloAtual = anguloAtual + 360;
		}
		setRotacao(new Vector3f(0, -anguloAtual + 180, 0));
	}

	public float getAnguloDeGiro() {
		return anguloDeGiro;
	}

	public void setAnguloDeGiro(float anguloDeGiro) {
		this.anguloDeGiro = anguloDeGiro;
	}

	public float getVelocidadeAtual() {
		return velocidadeAtual;
	}

	public void setVelocidadeAtual(float velocidadeAtual) {
		this.velocidadeAtual = velocidadeAtual;
	}

}
