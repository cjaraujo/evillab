package modelos;

public class ModeloDeLinha {
	private int vaoId;
	private int quantidadeVertex;
	public ModeloDeLinha(int vaoId, int quantidadeVertex) {
		this.vaoId = vaoId;
		this.quantidadeVertex = quantidadeVertex;
	}
	public int getVaoId() {
		return vaoId;
	}

	public int getQuantidadeVertex() {
		return quantidadeVertex;
	}
	
}

