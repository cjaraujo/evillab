package modelos;

import texturas.TexturaDeModelo;

public class ModeloTexturizado {
	private ModeloDeLinha modeloDeLinha;
	private TexturaDeModelo textura;

	public ModeloTexturizado(ModeloDeLinha modeloDeLinha, TexturaDeModelo modeloDeTextura) {
		this.modeloDeLinha = modeloDeLinha;
		this.textura = modeloDeTextura;
	}

	public ModeloDeLinha getModeloDeLinha() {
		return modeloDeLinha;
	}

	public TexturaDeModelo getTextura() {
		return textura;
	}
	
}
