package modelos;

import texturas.TexturaDeModelo;
import texturas.TexturaDeModeloComNormalMap;

public class ModeloTexturizadoComNormalMap extends ModeloTexturizado{

	public ModeloTexturizadoComNormalMap(ModeloDeLinha modeloDeLinha, TexturaDeModeloComNormalMap modeloDeTextura) {
		super(modeloDeLinha, modeloDeTextura);
		// TODO Auto-generated constructor stub
	}
	public TexturaDeModeloComNormalMap getTextura() {
		return (TexturaDeModeloComNormalMap) super.getTextura();
	}

}
