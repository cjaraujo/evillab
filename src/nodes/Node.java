package nodes;

import java.util.List;
import java.util.Map;

import org.lwjgl.util.vector.Vector3f;

import motorGrafico.MotorPrincipal;

public class Node extends Raiz {
	private Raiz pai;
	private String chave;
	private String nome;
	private Map<String, Boolean> grupos;

	
	public Node(String chave, String nome, Raiz pai, boolean ativo) {
		this.pai = pai;
		super.setRaiz(pai.getRaiz());
		this.chave = chave;
		this.nome = nome;
		super.setAtivo(ativo);
	}

	public Node(String chave, String nome, Raiz pai) {
		super.setRaiz(pai.getRaiz());
		this.pai = pai;;
		
		this.chave = chave;
		this.nome = nome;
		super.setAtivo(true);
	}
	public void preparar() {
		
	}

	public void processar(float delta) {

	}

	public Raiz getPai() {
		return pai;
	}

	public String getChave() {
		return chave;
	}

	public String getNome() {
		return nome;
	}

	public void adicionarAoGrupo(String grupo) {
		this.grupos.put(grupo, true);
	}

	public boolean estaNoGrupo(String grupo) {
		return this.grupos.get(grupo);
	}

	public Boolean getAtivo() {
		return (boolean) this.pai.getAtivo()&&super.getAtivo();
	}

	public void adicionarFilho(Node filho) {
		getPai().adicionarFilho(filho);
		super.adicionarFilho(filho);
	}

	public void setAtivo(Boolean ativo) {
		
		super.setAtivo(ativo);
	}


	public MotorPrincipal getMotorPrincipal() {
		return this.getRaiz().getMotorPrincipal();
	}
	

}
