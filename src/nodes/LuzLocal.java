package nodes;

import org.lwjgl.util.vector.Vector3f;

public class LuzLocal extends Luz{
	private Vector3f atenuacao;
	public LuzLocal(String chave, String nome, Raiz pai, boolean ativo, Vector3f posicao, Vector3f cor,Vector3f atenuacao) {
		super(chave, nome, pai, ativo, posicao, cor);
		this.atenuacao = atenuacao;
	}
	public LuzLocal(String chave, String nome, Raiz pai, Vector3f posicao, Vector3f cor,Vector3f atenuacao) {
		super(chave, nome, pai, posicao, cor);
		this.atenuacao = atenuacao;
	}
	public LuzLocal(String chave, String nome, Raiz pai, boolean ativo, Vector3f posicao, Vector3f cor,Vector3f atenuacao,float intensidade) {
		super(chave, nome, pai, ativo, posicao, cor,intensidade);
		this.atenuacao = atenuacao;
	}
	public LuzLocal(String chave, String nome, Raiz pai, Vector3f posicao, Vector3f cor,Vector3f atenuacao,float intensidade) {
		super(chave, nome, pai, posicao, cor,intensidade);
		this.atenuacao = atenuacao;
	}
	public Vector3f getAtenuacao() {
		return atenuacao;
	}
	

}
