package nodes;

import org.lwjgl.util.vector.Vector2f;

import texturas.Textura;

public class Gui extends Node{
	private Vector2f posicao;
	private Vector2f escala;
	private Textura textura;
	public Gui(String chave, String nome, Raiz pai, boolean ativo,Vector2f posicao,Vector2f escala, Textura textura) {
		super(chave, nome, pai, ativo);
		this.posicao = posicao;
		this.escala = escala;
		this.textura = textura;
	}
	public Gui(String chave, String nome, Raiz pai,Vector2f posicao,Vector2f escala, Textura textura) {
		super(chave, nome, pai);
		this.posicao = posicao;
		this.escala = escala;
		this.textura = textura;
	}
	public Vector2f getPosicao() {
		return posicao;
	}
	public Vector2f getEscala() {
		return escala;
	}
	public Textura getTextura() {
		return textura;
	}
	
	
	

}
