package nodes;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.util.vector.Vector3f;

public class Colisao extends EntidadeComplexa{
	protected Vector3f normal = new Vector3f(0,0,0);
	protected List<Colisao> colisoes = new ArrayList<Colisao>();
	protected boolean monitoramentoAtivo;
	protected boolean monitoradoAtivo;
	protected int[] camadasMonitoramento;
	protected int[] camadasMonitorado;
	
	public Colisao(String chave, String nome, Raiz pai, boolean ativo, Vector3f posicao, Vector3f rotacao,
			float escala) {
		super(chave, nome, pai, ativo, posicao, rotacao, escala);
		// TODO Auto-generated constructor stub
	}
	public Colisao(String chave, String nome, Raiz pai, Vector3f posicao, Vector3f rotacao,
			float escala) {
		super(chave, nome, pai, posicao, rotacao, escala);
		// TODO Auto-generated constructor stub
	}
	public void executarFisica() {
		
	}
	public void colidir(Colisao colisor) {
		colisoes.add(colisor);
	}
	

}
