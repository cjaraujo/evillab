package nodes;

import org.lwjgl.util.vector.Vector3f;

import modelos.ModeloTexturizado;
import modelos.ModeloTexturizadoComNormalMap;

public class EntidadeModeladaComNormalMap extends EntidadeModelada{

	public EntidadeModeladaComNormalMap(String chave, String nome, Raiz pai, boolean ativo, Vector3f posicao,
			Vector3f rotacao, ModeloTexturizadoComNormalMap modelo, float escala) {
		super(chave, nome, pai, ativo, posicao, rotacao, modelo, escala);
	}
	public EntidadeModeladaComNormalMap(String chave, String nome, Raiz pai, Vector3f posicao,
			Vector3f rotacao, ModeloTexturizadoComNormalMap modelo, float escala) {
		super(chave, nome, pai, posicao, rotacao, modelo, escala);
	}
	public EntidadeModeladaComNormalMap(String chave, String nome, Raiz pai, boolean ativo, Vector3f posicao,
			Vector3f rotacao, ModeloTexturizadoComNormalMap modelo, float escala,int indexDeTextura) {
		super(chave, nome, pai, ativo, posicao, rotacao, modelo, escala,indexDeTextura);
	}
	public EntidadeModeladaComNormalMap(String chave, String nome, Raiz pai, Vector3f posicao,
			Vector3f rotacao, ModeloTexturizadoComNormalMap modelo, float escala,int indexDeTextura) {
		super(chave, nome, pai, posicao, rotacao, modelo, escala,indexDeTextura);
	}
	public ModeloTexturizadoComNormalMap getModeloTexturizado() {
		return (ModeloTexturizadoComNormalMap) super.getModeloTexturizado();
	}

}
