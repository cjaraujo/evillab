package nodes;

import org.lwjgl.util.vector.Vector3f;

public class EntidadeSimples extends Node {
	private Vector3f posicaoGlobal = new Vector3f(0,0,0);
	private Vector3f posicao = new Vector3f(0,0,0);
	public EntidadeSimples(String chave, String nome, Raiz pai,Vector3f posicao) {
		super(chave, nome, pai);
		this.posicao = posicao;
		carregarPosicaoGlobal();
	}
	public EntidadeSimples(String chave, String nome, Raiz pai, boolean ativo,Vector3f posicao) {
		super(chave, nome, pai,ativo);
		this.posicao = posicao;
		carregarPosicaoGlobal();
	}
	public Vector3f getPosicao() {
		return posicao;
	}
	public void setPosicao(Vector3f posicao) {
		this.posicao = posicao;
	}
	public void mudarPosicao(float dx, float dy, float dz) {
		this.posicao.x += dx;
		this.posicao.y += dy;
		this.posicao.z += dz;
		
	}
	public void  carregarPosicaoGlobal() {
		if (!(super.getPai() instanceof EntidadeSimples)) {
			setPosicaoGlobal(getPosicao());
		} else {
			setPosicaoGlobal(new Vector3f(getPosicao().x + ((EntidadeSimples) super.getPai()).getPosicaoGlobal().x,
					getPosicao().y + ((EntidadeSimples) super.getPai()).getPosicaoGlobal().y,
					getPosicao().z + ((EntidadeSimples) super.getPai()).getPosicaoGlobal().z));
		}

	}
	public Vector3f getPosicaoGlobal() {
		return posicaoGlobal;
	}

	public void setPosicaoGlobal(Vector3f posicaoGlobal) {
		this.posicaoGlobal = posicaoGlobal;
	}
		
}
