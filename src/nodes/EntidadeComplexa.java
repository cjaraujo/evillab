package nodes;

import org.lwjgl.util.vector.Matrix3f;
import org.lwjgl.util.vector.Vector3f;
import java.lang.Math;

public class EntidadeComplexa extends EntidadeEscalar {
	private Vector3f rotacao;
	private Vector3f rotacaoGlobal = new Vector3f();
	public EntidadeComplexa(String chave, String nome, Raiz pai, Vector3f posicao, Vector3f rotacao,float escala) {
		super(chave, nome, pai, posicao,escala);
		this.rotacao = rotacao;
		carregarPosicaoGlobal();
		carregarEscalaGlobal();
		carregarRotacaoGlobal();

	}

	public EntidadeComplexa(String chave, String nome, Raiz pai, boolean ativo, Vector3f posicao, Vector3f rotacao,float escala) {
		super(chave, nome, pai, ativo, posicao,escala);
		this.rotacao = rotacao;
		carregarEscalaGlobal();
		carregarRotacaoGlobal();
		carregarPosicaoGlobal();
	}

	public Vector3f getRotacao() {
		return rotacao;
	}

	public void mudarRotacao(float dx, float dy, float dz) {
		this.rotacao.x += dx;
		this.rotacao.y += dy;
		this.rotacao.z += dz;
	}

	public void carregarPosicaoGlobal() {
		if (!(super.getPai() instanceof EntidadeSimples)) {
			super.setPosicaoGlobal(super.getPosicao());
			return;
		} else if (!(super.getPai() instanceof EntidadeEscalar)) {
			super.setPosicaoGlobal(
					new Vector3f(super.getPosicao().x + ((EntidadeSimples) super.getPai()).getPosicao().x,
							super.getPosicao().y + ((EntidadeSimples) super.getPai()).getPosicao().y,
							super.getPosicao().z + ((EntidadeSimples) super.getPai()).getPosicao().z));
			return;
		}else if(!(super.getPai() instanceof EntidadeComplexa)){
			super.setPosicaoGlobal(
					new Vector3f((super.getPosicao().x + ((EntidadeSimples) super.getPai()).getPosicao().x)*((EntidadeEscalar) getPai()).getEscalaGlobal(),
							(super.getPosicao().y + ((EntidadeSimples) super.getPai()).getPosicao().y)*((EntidadeEscalar) getPai()).getEscalaGlobal(),
							(super.getPosicao().z + ((EntidadeSimples) super.getPai()).getPosicao().z)*((EntidadeEscalar) getPai()).getEscalaGlobal()));
			return;
		}
		Vector3f posicaoRotacionada = new Vector3f((super.getPosicao().x)*((EntidadeEscalar) getPai()).getEscalaGlobal(), (super.getPosicao().y)*((EntidadeEscalar) getPai()).getEscalaGlobal(), (super.getPosicao().z)*((EntidadeEscalar) getPai()).getEscalaGlobal());

		float rotacaoZ = ((EntidadeComplexa) super.getPai()).getRotacaoGlobal().z;
		if (rotacaoZ != 0) {
			float novoX = (float) ((float) (posicaoRotacionada.x * Math.cos(Math.toRadians(rotacaoZ)))
					- (float) (posicaoRotacionada.y * Math.sin(Math.toRadians(rotacaoZ))));
			float novoY = (float) ((float) (posicaoRotacionada.x * Math.sin(Math.toRadians(rotacaoZ)))
					+ (float) (posicaoRotacionada.y * Math.cos(Math.toRadians(rotacaoZ))));
			posicaoRotacionada.x = novoX;
			posicaoRotacionada.y = novoY;
		}
		float rotacaoY = ((EntidadeComplexa) super.getPai()).getRotacaoGlobal().y;
		if (rotacaoY != 0) {
			float novoZ = (float) ((float) (posicaoRotacionada.z * Math.cos(Math.toRadians(rotacaoY)))
					- (float) (posicaoRotacionada.x * Math.sin(Math.toRadians(rotacaoY))));
			float novoX = (float) ((float) (posicaoRotacionada.z * Math.sin(Math.toRadians(rotacaoY)))
					+ (float) (posicaoRotacionada.x * Math.cos(Math.toRadians(rotacaoY))));
			posicaoRotacionada.z = novoZ;
			posicaoRotacionada.x = novoX;
		}
		float rotacaoX = ((EntidadeComplexa) super.getPai()).getRotacaoGlobal().x;
		if (rotacaoX != 0) {
			float novoY = (float) ((float) (posicaoRotacionada.y * Math.cos(Math.toRadians(rotacaoX)))
					- (float) (posicaoRotacionada.z * Math.sin(Math.toRadians(rotacaoX))));
			float novoZ = (float) ((float) (posicaoRotacionada.y * Math.sin(Math.toRadians(rotacaoX)))
					+ (float) (posicaoRotacionada.z * Math.cos(Math.toRadians(rotacaoX))));
			posicaoRotacionada.z = novoZ;
			posicaoRotacionada.y = novoY;
		}

		super.setPosicaoGlobal(
				new Vector3f(posicaoRotacionada.x + ((EntidadeSimples) super.getPai()).getPosicaoGlobal().x,
						posicaoRotacionada.y + ((EntidadeSimples) super.getPai()).getPosicaoGlobal().y,
						posicaoRotacionada.z + ((EntidadeSimples) super.getPai()).getPosicaoGlobal().z));
		

	}

	public void carregarRotacaoGlobal() {
		if (!(super.getPai() instanceof EntidadeComplexa)) {

			this.rotacaoGlobal = this.rotacao;
			return;
		}

		this.rotacaoGlobal = new Vector3f(((EntidadeComplexa) super.getPai()).getRotacaoGlobal().x + rotacao.x,
				((EntidadeComplexa) super.getPai()).getRotacaoGlobal().y + rotacao.y,
				((EntidadeComplexa) super.getPai()).getRotacaoGlobal().z + rotacao.z);
	}

	public void setRotacao(Vector3f rotacao) {
		this.rotacao = rotacao;
	}

	public Vector3f getRotacaoGlobal() {
		return rotacaoGlobal;
	}

	public void setRotacaoGlobal(Vector3f rotacaoGlobal) {
		this.rotacaoGlobal = rotacaoGlobal;
	}
	

}
