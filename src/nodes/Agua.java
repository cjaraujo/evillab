package nodes;

import org.lwjgl.util.vector.Vector3f;

public class Agua extends EntidadeSimples{
	private float tamanho;
	public Agua(String chave, String nome, Raiz pai, Vector3f posicao,float tamanho) {
		super(chave, nome, pai, posicao);
		this.tamanho = tamanho;
	}
	public Agua(String chave, String nome, Raiz pai, boolean ativo, Vector3f posicao,float tamanho) {
		super(chave, nome, pai, ativo, posicao);
		this.tamanho = tamanho;
	}
	public float getTamanho() {
		return tamanho;
	}
	

}
