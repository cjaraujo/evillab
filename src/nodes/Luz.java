package nodes;

import org.lwjgl.util.vector.Vector3f;

public class Luz extends EntidadeSimples{
	private Vector3f cor;
	private float intensidade = 1;
	public Luz(String chave, String nome, Raiz pai, boolean ativo, Vector3f posicao,Vector3f cor) {
		super(chave, nome, pai, ativo, posicao);
		this.cor = cor;
	}
	public Luz(String chave, String nome, Raiz pai, Vector3f posicao,Vector3f cor) {
		super(chave, nome, pai, posicao);
		this.cor = cor;
	}
	public Luz(String chave, String nome, Raiz pai, boolean ativo, Vector3f posicao,Vector3f cor,float intensidade) {
		super(chave, nome, pai, ativo, posicao);
		this.cor = cor;
		this.intensidade = intensidade;
	}
	public Luz(String chave, String nome, Raiz pai, Vector3f posicao,Vector3f cor,float intensidade) {
		super(chave, nome, pai, posicao);
		this.cor = cor;
		this.intensidade = intensidade;
	}

	public void setCor(Vector3f cor) {
		this.cor = cor;
	}
	public void andarParaEsquerda() {
		super.getPosicao().x -= 5f;
	}

	public Vector3f getCor() {
		return cor;
	}
	public float getIntensidade() {
		return intensidade;
	}

}
