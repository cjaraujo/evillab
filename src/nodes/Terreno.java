package nodes;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

import ferramentas.Calculos;
import modelos.ModeloDeLinha;
import motorGrafico.Carregador;
import texturas.PackDeTexturaDeTerreno;
import texturas.Textura;

public class Terreno extends Node {

	private static final float COR_DE_PIXEL_MAXIMA = 256 * 256 * 256;

	private float x;
	private float z;
	private float[][] alturas;
	private ModeloDeLinha modelo;
	private PackDeTexturaDeTerreno packDeTexturaDeTerro;
	private Textura mapaBlend;
	private float tamanho = 800;
	private float alturaMaxima = 40;

	public Terreno(String chave, String nome, Raiz pai, boolean ativo, int gridX, int gridZ,
			PackDeTexturaDeTerreno packDeTexturaDeTerro, Textura mapaBlend, String mapaDeAltura) {
		super(chave, nome, pai, ativo);
		this.packDeTexturaDeTerro = packDeTexturaDeTerro;
		this.mapaBlend = mapaBlend;
		this.x = gridX*this.tamanho;
		this.z = gridZ * this.tamanho;
		this.modelo = gerarTerreno(super.getPai().getMotorPrincipal().getCarregador(),mapaDeAltura);
	}

	public Terreno(String chave, String nome, Raiz pai, Terreno terreno, int gridX, int gridZ,
			PackDeTexturaDeTerreno packDeTexturaDeTerro, Textura mapaBlend, String mapaDeAltura) {
		super(chave, nome, pai);
		this.packDeTexturaDeTerro = packDeTexturaDeTerro;
		this.mapaBlend = mapaBlend;
		this.x = gridX*this.tamanho;
		this.z = gridZ * this.tamanho;
		this.modelo = gerarTerreno(super.getPai().getMotorPrincipal().getCarregador(),mapaDeAltura);

	}
	private ModeloDeLinha gerarTerreno(Carregador loader,String mapaDeAlturas){
		BufferedImage imagem = null;
		try {
			imagem = ImageIO.read(new File("res/"+mapaDeAlturas+".png"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int QUANTIDADE_VERTEX = imagem.getHeight();
		
		int count = QUANTIDADE_VERTEX * QUANTIDADE_VERTEX;
		float[] vertices = new float[count * 3];
		float[] normals = new float[count * 3];
		float[] textureCoords = new float[count*2];
		int[] indices = new int[6*(QUANTIDADE_VERTEX-1)*(QUANTIDADE_VERTEX-1)];
		alturas = new float[QUANTIDADE_VERTEX][QUANTIDADE_VERTEX];
		int vertexPointer = 0;
		for(int i=0;i<QUANTIDADE_VERTEX;i++){
			for(int j=0;j<QUANTIDADE_VERTEX;j++){
				vertices[vertexPointer*3] = (float)j/((float)QUANTIDADE_VERTEX - 1) * this.tamanho;
				float altura = getAltura(j, i, imagem);
				alturas[j][i] = altura;
				vertices[vertexPointer*3+1] = altura;
				vertices[vertexPointer*3+2] = (float)i/((float)QUANTIDADE_VERTEX - 1) * this.tamanho;
				Vector3f normal = obterNormal(j,i, imagem);
				normals[vertexPointer*3] = normal.x;
				normals[vertexPointer*3+1] =normal.y;
				normals[vertexPointer*3+2] = normal.z;
				textureCoords[vertexPointer*2] = (float)j/((float)QUANTIDADE_VERTEX - 1);
				textureCoords[vertexPointer*2+1] = (float)i/((float)QUANTIDADE_VERTEX - 1);
				vertexPointer++;
			}
		}
		int pointer = 0;
		for(int gz=0;gz<QUANTIDADE_VERTEX-1;gz++){
			for(int gx=0;gx<QUANTIDADE_VERTEX-1;gx++){
				int topLeft = (gz*QUANTIDADE_VERTEX)+gx;
				int topRight = topLeft + 1;
				int bottomLeft = ((gz+1)*QUANTIDADE_VERTEX)+gx;
				int bottomRight = bottomLeft + 1;
				indices[pointer++] = topLeft;
				indices[pointer++] = bottomLeft;
				indices[pointer++] = topRight;
				indices[pointer++] = topRight;
				indices[pointer++] = bottomLeft;
				indices[pointer++] = bottomRight;
			}
		}
		return loader.carregarAoVAO(vertices, textureCoords, normals, indices);
	}
	public float getAlturaDoTerreno(float x, float z) {
		float xTerreno = x - this.x;
		float zTerreno = z -this.z;
		float quadradoGrid = this.tamanho /((float)alturas.length - 1);
		int xGrid = (int) Math.floor(xTerreno/quadradoGrid);
		int zGrid = (int) Math.floor(zTerreno/quadradoGrid);
		if(xGrid >=alturas.length - 1|| zGrid >=alturas.length - 1||xGrid<0||zGrid<0) {
			return 0;
		}
		float coordenadaX = (xTerreno%quadradoGrid)/quadradoGrid;
		float coordenadaZ = (zTerreno%quadradoGrid)/quadradoGrid;
		float retorno;
		if(coordenadaX<= (1-coordenadaZ)) {
			retorno = Calculos
					.bariCentro(new Vector3f(0, alturas[xGrid][zGrid], 0), new Vector3f(1,
							alturas[xGrid + 1][zGrid], 0), new Vector3f(0,
									alturas[xGrid][zGrid + 1], 1), new Vector2f(coordenadaX, coordenadaZ));
		}else {
			retorno = Calculos
					.bariCentro(new Vector3f(1, alturas[xGrid + 1][zGrid], 0), new Vector3f(1,
							alturas[xGrid + 1][zGrid + 1], 1), new Vector3f(0,
									alturas[xGrid][zGrid + 1], 1), new Vector2f(coordenadaX, coordenadaZ));
		}
		return retorno;
	}
	private float getAltura(int x, int z, BufferedImage imagem) {
		if(x<0||x>= imagem.getHeight()||z<0||z>=imagem.getHeight()) {
			return 0;
		}
		float altura = imagem.getRGB(x, z);
		altura+=COR_DE_PIXEL_MAXIMA/2f;
		altura /=COR_DE_PIXEL_MAXIMA/2f;
		altura *= this.alturaMaxima;
		return altura;
	}
	private Vector3f obterNormal(int x, int z, BufferedImage imagem) {
		float alturaEsquerda = getAltura(x-1, z, imagem);
		float alturaDireita = getAltura(x+1, z, imagem);
		float alturaCima = getAltura(x, z+1, imagem);
		float alturaBaixo = getAltura(x, z-1, imagem);
		Vector3f normal = new Vector3f(alturaEsquerda-alturaDireita,2f,alturaBaixo-alturaCima);
		normal.normalise();
		return normal;
	}
	
	public PackDeTexturaDeTerreno getPackDeTexturaDeTerro() {
		return packDeTexturaDeTerro;
	}

	public Textura getMapaBlend() {
		return mapaBlend;
	}

	public ModeloDeLinha getModelo() {
		return modelo;
	}
	public void setModelo(ModeloDeLinha modelo) {
		this.modelo = modelo;
	}
	
	public float getX() {
		return x;
	}
	public void setX(float x) {
		this.x = x;
	}
	public float getZ() {
		return z;
	}
	public void setZ(float z) {
		this.z = z;
	}

}
