package nodes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.util.vector.Vector3f;

import modelos.ModeloTexturizado;
import modelos.ModeloTexturizadoComNormalMap;
import motorGrafico.MotorPrincipal;
import texturas.TexturaDeModelo;

public class Raiz {
	private Map<String, Node> filhos = new HashMap<String, Node>();
	private Node nodePrincipal;
	private MotorPrincipal motorPrincipal;
	private Boolean ativo;
	private Raiz raiz;
	private boolean centralizarMouse = true;
	private Map<ModeloTexturizado, List<EntidadeModelada>> filhosParaRendenizar = new HashMap<ModeloTexturizado, List<EntidadeModelada>>();
	private Map<ModeloTexturizadoComNormalMap, List<EntidadeModeladaComNormalMap>> filhosParaRendenizarComNormalMap = new HashMap<ModeloTexturizadoComNormalMap, List<EntidadeModeladaComNormalMap>>();

	private List<Node> filhosParaCalcular = new ArrayList<Node>();
	private List<Gui> guis = new ArrayList<Gui>();
	private List<Luz> luzes = new ArrayList<Luz>();
	private List<Agua> aguas = new ArrayList<Agua>();
	private List<Terreno> terrenos = new ArrayList<Terreno>();

	public Raiz(MotorPrincipal motorPrincipal) {
		this.motorPrincipal = motorPrincipal;
		this.ativo = true;
		this.raiz = this;
	}

	public Raiz() {

	}

	public void processar(float delta) {
		if (filhos.isEmpty()) {
			return;
		}
		for (Node node : filhos.values()) {
			if (!node.getAtivo()) {
				continue;
			}
			node.processar(delta);

		}
		for (Node node : filhosParaCalcular) {
			if (!node.getAtivo()) {
				continue;
			}
			if (node instanceof EntidadeEscalar) {
				((EntidadeEscalar) node).carregarEscalaGlobal();
			}
			if (node instanceof EntidadeComplexa) {
				((EntidadeComplexa) node).carregarRotacaoGlobal();
			}

			((EntidadeSimples) node).carregarPosicaoGlobal();
		}
		if (centralizarMouse) {
			Mouse.setCursorPosition(Display.getWidth() / 2, Display.getHeight() / 2);
		}
	}

	protected Node getNodePrincipal() {
		return nodePrincipal;
	}

	public void setNodePrincipal(Node nodePrincipal) {
		this.nodePrincipal = nodePrincipal;
	}

	public void adicionarFilho(Node filho) {
		this.filhos.put(filho.getChave(), filho);
		if (filho instanceof EntidadeModelada) {
			if (filho instanceof EntidadeModeladaComNormalMap) {
				if (!filhosParaRendenizarComNormalMap.containsKey(((EntidadeModeladaComNormalMap) filho).getModeloTexturizado())) {
					filhosParaRendenizarComNormalMap.put(((EntidadeModeladaComNormalMap) filho).getModeloTexturizado(),
							new ArrayList<EntidadeModeladaComNormalMap>());
				}
				(filhosParaRendenizarComNormalMap.get(((EntidadeModeladaComNormalMap) filho).getModeloTexturizado()))
						.add((EntidadeModeladaComNormalMap) filho);
			} else {
				if (!filhosParaRendenizar.containsKey(((EntidadeModelada) filho).getModeloTexturizado())) {
					filhosParaRendenizar.put(((EntidadeModelada) filho).getModeloTexturizado(),
							new ArrayList<EntidadeModelada>());
				}
				(filhosParaRendenizar.get(((EntidadeModelada) filho).getModeloTexturizado()))
						.add((EntidadeModelada) filho);
			}
		}
		if (filho instanceof EntidadeSimples) {
			this.filhosParaCalcular.add(filho);
		}
		if (filho instanceof Terreno) {
			this.terrenos.add((Terreno) filho);
		}
		if (filho instanceof Gui) {
			this.guis.add((Gui) filho);
		}
		if (filho instanceof Luz) {
			luzes.add((Luz) filho);
		}
		if (filho instanceof Agua) {
			aguas.add((Agua) filho);
		}
		filho.preparar();

	}

	public List<Agua> getAguas() {
		return aguas;
	}
	


	public Map<ModeloTexturizadoComNormalMap, List<EntidadeModeladaComNormalMap>> getFilhosParaRendenizarComNormalMap() {
		return filhosParaRendenizarComNormalMap;
	}

	public Map<String, Node> getFilhos() {
		return filhos;
	}

	public Map<ModeloTexturizado, List<EntidadeModelada>> getFilhosParaRendenizar() {
		return filhosParaRendenizar;
	}

	public List<Node> getFilhosParaCalcular() {
		return filhosParaCalcular;
	}

	public List<Gui> getGuis() {
		return guis;
	}

	public List<Luz> getLuzes() {
		return luzes;
	}

	public List<Terreno> getTerrenos() {
		return terrenos;
	}

	protected Node getFilhoPorChave(String chave) {
		return filhos.get(chave);
	}

	protected Boolean getAtivo() {
		return ativo;
	}

	protected void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public MotorPrincipal getMotorPrincipal() {
		return motorPrincipal;
	}

	public Raiz getRaiz() {
		return raiz;
	}

	public void setRaiz(Raiz raiz) {
		this.raiz = raiz;
	}

}
