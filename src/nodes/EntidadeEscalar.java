package nodes;

import org.lwjgl.util.vector.Vector3f;

public class EntidadeEscalar extends EntidadeSimples{
	private Float escala;
	private Float escalaGlobal;
	public EntidadeEscalar(String chave, String nome, Raiz pai, boolean ativo, Vector3f posicao,float escala) {
		super(chave, nome, pai, ativo, posicao);
		this.escala = escala;
		carregarPosicaoGlobal();
		carregarEscalaGlobal();
	}
	public EntidadeEscalar(String chave, String nome, Raiz pai, Vector3f posicao,float escala) {
		super(chave, nome, pai, posicao);
		this.escala = escala;
		carregarPosicaoGlobal();
		carregarEscalaGlobal();
	}
	public void  carregarPosicaoGlobal() {
		if (!(super.getPai() instanceof EntidadeSimples)) {
			super.setPosicaoGlobal(getPosicao());
			return;
		} else if(!(getPai() instanceof EntidadeEscalar)) {
			super.setPosicaoGlobal(new Vector3f(getPosicao().x + ((EntidadeSimples) super.getPai()).getPosicaoGlobal().x,
					getPosicaoGlobal().y + ((EntidadeSimples) super.getPai()).getPosicaoGlobal().y,
					getPosicaoGlobal().z + ((EntidadeSimples) super.getPai()).getPosicaoGlobal().z));
			return;
		}
		super.setPosicaoGlobal(new Vector3f((getPosicao().x + ((EntidadeSimples) super.getPai()).getPosicaoGlobal().x)*((EntidadeEscalar) getPai()).getEscalaGlobal(),
				(getPosicaoGlobal().y + ((EntidadeSimples) super.getPai()).getPosicaoGlobal().y)*((EntidadeEscalar) getPai()).getEscalaGlobal(),
				(getPosicaoGlobal().z + ((EntidadeSimples) super.getPai()).getPosicaoGlobal().z)*((EntidadeEscalar) getPai()).getEscalaGlobal()));
	}
	public void carregarEscalaGlobal() {
		if(getPai() instanceof EntidadeEscalar) {
			this.escalaGlobal = this.escala*((EntidadeEscalar) getPai()).getEscalaGlobal();
			return;
		}
		this.escalaGlobal = this.escala;
	}
	public Float getEscala() {
		return escala;
	}

	public Float getEscalaGlobal() {
		return escalaGlobal;
	}
	public void setEscalaGlobal(Float escalaGlobal) {
		this.escalaGlobal = escalaGlobal;
	}
	public void setEscala(Float escala) {
		this.escala = escala;
	}

}
