package nodes;

import org.lwjgl.util.vector.Vector3f;

import modelos.ModeloTexturizado;

public class EntidadeModelada extends EntidadeComplexa {
	private ModeloTexturizado modelo;
	private int indexDeTextura = 0;
	
	public EntidadeModelada(String chave, String nome,Raiz pai,boolean ativo,  Vector3f posicao,Vector3f rotacao, ModeloTexturizado modelo,float escala) {
		super(chave, nome,pai, ativo,posicao,rotacao,escala);
		this.modelo = modelo;

	}

	public EntidadeModelada(String chave, String nome,Raiz pai,  Vector3f posicao,Vector3f rotacao, ModeloTexturizado modelo,float escala,int indexDeTextura) {
		super(chave, nome,pai,posicao,rotacao,escala);
		this.modelo = modelo;
		this.indexDeTextura = indexDeTextura;

	}
	public EntidadeModelada(String chave, String nome,Raiz pai,  Vector3f posicao,Vector3f rotacao, ModeloTexturizado modelo,float escala) {
		super(chave, nome,pai,posicao,rotacao,escala);
		this.modelo = modelo;

	}
	public EntidadeModelada(String chave, String nome,Raiz pai, boolean ativo, Vector3f posicao,Vector3f rotacao, ModeloTexturizado modelo,float escala,int indexDeTextura) {
		super(chave, nome,pai,ativo,posicao,rotacao,escala);
		this.modelo = modelo;
		this.indexDeTextura = indexDeTextura;

	}
	public float getColuna() {
		int coluna = indexDeTextura%modelo.getTextura().getNumeroDeColunas();
		return (float)coluna/(float)modelo.getTextura().getNumeroDeColunas();
	}
	public float getLinha() {
		int linha = indexDeTextura/modelo.getTextura().getNumeroDeColunas();
		return (float)linha/(float)modelo.getTextura().getNumeroDeColunas();
	}

	public ModeloTexturizado getModeloTexturizado() {
		return modelo;
	}




}
