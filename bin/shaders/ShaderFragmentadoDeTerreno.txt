#version 400 core
in vec2 pass_textureCoords;
in vec3 surfaceNormal;
in vec3 toLightVector[4];
in vec3 toCameraVector;
in float visibility;
in vec4 shadowCoords;
out vec4 out_Colour;

uniform sampler2D backgroundTexture;
uniform sampler2D rTexture;
uniform sampler2D gTexture;
uniform sampler2D bTexture;
uniform sampler2D blendMap;
uniform sampler2D shadowMap;
uniform vec3 lightColour[4];
uniform vec3 atenuacao[4];
uniform float intensidade[4];
uniform float shineDamper;
uniform float reflectivity;

uniform vec3 skyColour;
const float levels = 3;
void main(void){
	float objectNearestLight = texture(shadowMap,shadowCoords.xy).r;
	float lightFactor = 1.0;
	if(shadowCoords.z >objectNearestLight){
		lightFactor = 1.0-(shadowCoords.w *0.4);
	}
	vec4 blendMapColour = texture(blendMap,pass_textureCoords);
	float backTextureAmount = 1 - (blendMapColour.r +blendMapColour.g + blendMapColour.b);
	vec2 tileCoords = pass_textureCoords * 40.0;
	vec4 backgroundTextureColor = texture(backgroundTexture,tileCoords)*backTextureAmount;
	vec4 rTextureColour = texture(rTexture,tileCoords)*blendMapColour.r;
	vec4 gTextureColour = texture(gTexture,tileCoords)*blendMapColour.g;
	vec4 bTextureColour = texture(bTexture,tileCoords)*blendMapColour.b;
	vec4 totalColour = (backgroundTextureColor + rTextureColour + gTextureColour + bTextureColour);
	vec3 unitNormal = normalize(surfaceNormal);
	vec3 unitVectorToCamera = normalize(toCameraVector);
	vec3 totalDifuse = vec3(0.0);
	vec3 totalEspecular = vec3(0.0);

	for(int i = 0;i<4;i++){
		float distancia = length(toLightVector[i]);
		float fatorAtenuacao = atenuacao[i].x + (atenuacao[i].y * distancia) +(atenuacao[i].z * distancia*distancia);
		vec3 unitLightVector = normalize(toLightVector[i]);
		float nDotl = dot(unitNormal,unitLightVector);
		float brightness = max(nDotl,0.2);
		vec3 lightDirection = -unitLightVector;
		vec3 reflectedLightDirection = reflect(lightDirection,unitNormal);
		float specularFactor = dot(reflectedLightDirection,unitVectorToCamera);
		specularFactor = max(specularFactor,0.0);
		float dampedFactor = pow(specularFactor, shineDamper);
		totalDifuse = totalDifuse + ((brightness * lightColour[i])/fatorAtenuacao)*intensidade[i];
		totalEspecular = totalEspecular + ((dampedFactor * reflectivity * lightColour[i])/fatorAtenuacao)*intensidade[i];
	}
	totalDifuse = max(totalDifuse, 0.1)*lightFactor;
	out_Colour = (vec4(totalDifuse,1.0) * totalColour) + vec4(totalEspecular,1.0);
	
}